import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'

admin.initializeApp();

exports.newUnseenMessageFromNew = functions.database.ref('/wasnotseen').onCreate((snapshot)=>{
    const key= Object.keys(snapshot.val())[0];
    console.log(snapshot);
    const content = {
        notification:{
            title: 'New Message',
            body: 'Have an unseen Message',
            sound: "default"
        }
    };
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            admin.database().ref('/wasnotseen/' + key).once('value').then((snapshot) => {
                console.log(snapshot.val());
                if (snapshot.val() !== null && snapshot.val().type != 'Worker') {
                    resolve(admin.messaging().sendToTopic('allUser', content));
                }
            })
        }, 3000);
    });
});
// exports.newUnseenMessageFromExist = functions.database.ref('/wasnotseen').onUpdate((snapshot)=>{
//     const content = {
//         notification:{
//             title: 'New Message',
//             body: 'Have an unseen Message',
//             sound: "default"
//         }
//     };
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             admin.database().ref('/wasnotseen/').once('value').then((snapshot) => {
//                 console.log(snapshot);
//                 // if (snapshot.val() !== null && snapshot.val()[0].type != 'Worker') {
//                 //     resolve(admin.messaging().sendToTopic('allUser', content));
//                 // }
//             })
//         }, 3000);
//     });
// });