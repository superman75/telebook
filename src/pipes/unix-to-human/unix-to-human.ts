import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment'

/**
 * Generated class for the UnixToHumanPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'unixToHuman',
})
export class UnixToHumanPipe implements PipeTransform {
  /**
   * Convert unix time to human
   */
  transform(value) {
    return moment.unix(value).format('MMM Do YY');
  }
}
