import { NgModule } from '@angular/core';
import { TextNowhiteSpacePipe } from './text-nowhite-space/text-nowhite-space';
import { UnixToHumanPipe } from './unix-to-human/unix-to-human';
import { DateToFormatPipe } from './date-to-format/date-to-format';
import { SanitizePipe } from './sanitize/sanitize';
@NgModule({
	declarations: [TextNowhiteSpacePipe,
    UnixToHumanPipe,
    DateToFormatPipe,
    SanitizePipe],
	imports: [],
	exports: [TextNowhiteSpacePipe,
    UnixToHumanPipe,
    DateToFormatPipe,
    SanitizePipe]
})
export class PipesModule {}
