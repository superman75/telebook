import { Pipe, PipeTransform } from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";

/**
 * Generated class for the SanitizePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'sanitize',
})
export class SanitizePipe implements PipeTransform {
 constructor(private sanitize: DomSanitizer){}
  transform(value: string) {
    return this.sanitize.bypassSecurityTrustResourceUrl(value);
  }
}
