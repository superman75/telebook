import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the TextNowhiteSpacePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'textNowhiteSpace',
})
export class TextNowhiteSpacePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    return value.toLowerCase().split(' ').join('_');
  }
}
