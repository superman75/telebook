  import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment'
  import {isUndefined} from "ionic-angular/util/util";


@Pipe({
  name: 'dateToFormat',
})
export class DateToFormatPipe implements PipeTransform {
  transform(value: string, format?) {
    if(isUndefined(format)){
        return moment(value).format('D MMM, h:mm a');
    }else{
        return moment(value).format(format);
    }
  }
}
