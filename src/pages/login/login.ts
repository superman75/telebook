import { PassrecoveryPage, TabsPage } from '../pages';
import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, ToastController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup} from '@angular/forms'
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import { StorageServiceProvider } from "../../providers/storage-service/storage-service";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: null;
  password: null;

  id:null;
  name:null;
  token:null;

  recovery_page:any = PassrecoveryPage;
  home_page:any = TabsPage;
  validation_messages ={
    'email':[
        {type: 'required', message: 'The email field is required'},
        {type: 'email', message: 'The email is invalid'}
    ],
    'password':[
        {type: 'required', message: 'The password field is required'},
        {type: 'minlength', message: 'The password field is very short'}
    ]
  }
  form:FormGroup;
  constructor(public navCtrl: NavController,
              private formBuilder: FormBuilder,
              public userService: ApiServiceProvider,
              public loadCtrl: LoadingController,
              public toastCtrl: ToastController,
              private userInfo: StorageServiceProvider) {
    this.form = this.formBuilder.group({
        email: [null,[Validators.required,Validators.email]],
        password: [null,[Validators.required, Validators.minLength(6)]]
    })
  }
  navegarPagina(page){
    this.navCtrl.push(page)
  }
  submitForm(){
      const loader = this.loadCtrl.create({
          content: 'Plase wait...'
      });
      const toast = this.toastCtrl.create({
          message: 'ERROR, wrong credentials',
          cssClass: 'bg-danger',
          position: 'top',
          duration: 3000
      })
      loader.present();
      this.userService.login(this.email,this.password).then((data)=> {
          this.saveUserInfo(data);
          loader.dismiss();
          this.navCtrl.push(this.home_page);
      },(error) =>{
          loader.dismiss();
          toast.present();
      })
  }
  saveUserInfo(data){
      this.userInfo.save_storage(data,'user_info');
      this.userInfo.user_info = data;
  }
}
