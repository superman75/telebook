import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {ApiServiceProvider} from "../../../providers/api-service/api-service";
@IonicPage()
@Component({
  selector: 'page-groupdetails',
  templateUrl: 'groupdetails.html',
})
export class GroupdetailsPage {

  name: any;
  id: any;
  group_details: any = {admins:'',workers:'', groups:[{name:''}]};
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private apiServices: ApiServiceProvider,
              private loadingCtrl: LoadingController) {
    this.name = this.navParams.get('name');
    this.id = this.navParams.get('id');
    this.getGroupDetails(this.id);
  }
  getGroupDetails(id){
    let loading  = this.loadingCtrl.create({
        content: 'Loading, please wait...'
    });
    loading.present();
    this.apiServices.getGroupDetails(id).then(response=>{
      this.group_details = response;
      loading.dismiss();
    })
  }
  changeGroup(id, profileId){
      this.apiServices.changeGroup(id, profileId);
  }
}
