import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupdetailsPage } from './groupdetails';
import {ComponentsModule} from '../../../components/components.module'
@NgModule({
  declarations: [
    GroupdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupdetailsPage),
      ComponentsModule
  ],
})
export class GroupdetailsPageModule {}
