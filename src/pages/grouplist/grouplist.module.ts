import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GrouplistPage } from './grouplist';
import {ComponentsModule} from '../../components/components.module'
@NgModule({
  declarations: [
    GrouplistPage,
  ],
  imports: [
    IonicPageModule.forChild(GrouplistPage),
      ComponentsModule
  ],
})
export class GrouplistPageModule {}
