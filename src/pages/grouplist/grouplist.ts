import { Component } from '@angular/core';
import {IonicPage, LoadingController} from 'ionic-angular';
import {ApiServiceProvider} from "../../providers/api-service/api-service";
import {StorageServiceProvider} from "../../providers/storage-service/storage-service";
import {GroupdetailsPage} from "../pages";


@IonicPage()
@Component({
  selector: 'page-grouplist',
  templateUrl: 'grouplist.html',
})
export class GrouplistPage {

  groups:any;
  role:any;
  group_details_page: any = GroupdetailsPage;
  constructor(private apiService: ApiServiceProvider,
              private loaderCtrl: LoadingController,
              private storageService: StorageServiceProvider) {
    this.getRole();
    this.getGroup();
  }
  changeGroup(value){
    this.apiService.changeGroup(value);
  }
  getGroup(){
    let loading = this.loaderCtrl.create({
        content: 'Please wait...'
    });
    loading.present();
    this.apiService.getGroup().then(data=>{
     if(data){
       this.groups = data["groups"];
     }
      loading.dismiss();
    });
  }
  getRole(){
    this.storageService.load_storage('user_info').then(response=>{
      this.role = response["roles"][0]["id"];
    });
  }
}
