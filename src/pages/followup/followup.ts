import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {ApiServiceProvider} from "../../providers/api-service/api-service";
import {ChatPage} from "../pages";

@IonicPage()
@Component({
    selector: 'page-followup',
    templateUrl: 'followup.html',
})
export class FollowupPage {
    worker_id: any;
    token: any;
    node: any;

    internalReturn: any;
    internalKeys: any;
    did_number: any;
    person_name: any;

    externalReturn: any;
    externallKeys: any;
    external_conversation_id: any;
    priority: any;
    worker_person: any;
    customer_phone_number: any;

    constructor(public navCtrl: NavController, public navParams: NavParams,
                private apiService: ApiServiceProvider, private loaderCtrl: LoadingController) {
        this.init();
    }

    //modelos
    followupText = [];

    sendNode(node, worker_id, token) {
        this.token = token;
        this.node = node;
        this.worker_id = worker_id;
        this.getdatainternal_conversation(this.worker_id);
        this.getdataExternal_conversation();
    }

    getdatainternal_conversation(worker_id) {
        this.apiService.getUserMessenger().then((data) => {
            this.internalReturn = data;
            this.internalKeys = Object.keys(data);
            this.internalKeys.forEach(element => {
                if (this.internalReturn[element]["worker_id"] == worker_id) {
                    this.did_number = this.internalReturn[element]["did_number"];
                    this.person_name = this.internalReturn[element]["person_name"];
                }
            });
        }, error => {
            console.log(error);
        });
    }

    getdataExternal_conversation() {
        this.apiService.getListMessage(this.worker_id).then((data) => {
            this.externalReturn = data;
            this.externallKeys = Object.keys(data);
            this.externallKeys.forEach(element => {
                if (this.externalReturn[element]["node"] == this.node) {
                    this.external_conversation_id = this.externalReturn[element]["external_conversation_id"];
                    this.priority = this.externalReturn[element]["priority"];
                    this.worker_person = this.externalReturn[element]["worker_person"];
                    this.customer_phone_number = this.externalReturn[element]["customer_phone_number"];
                }
            });
            this.navCtrl.push(ChatPage, {
                'customer_phone_number': this.customer_phone_number,
                'node': this.node,
                'external_conversation_id': this.external_conversation_id,
                'worker_id': this.worker_id,
                'priority_id': this.priority,
                'person_name': this.person_name,
                'did_number': this.did_number,
                'worker_person': this.worker_person,
                'origin': this.token,
                'scroll-content': 110
            })
        }, error => {
            console.log(error);
        });
    }

    init() {
        this.followupText = [];
        let loading = this.loaderCtrl.create({
            content: 'Loading, please wait...'
        });
        loading.present();
        this.apiService.getFollowup().then(data => {
            Object.keys(data["follow_ups"]).forEach((key) => {
                this.followupText.push(
                    data["follow_ups"][key].text + '$-$' + //0
                    data["follow_ups"][key].node + '$-$' + //1
                    data["follow_ups"][key].worker_id + '$-$' + //2
                    data["follow_ups"][key].id + '$-$' + //3
                    data["follow_ups"][key].token); //4
            });
            loading.dismiss();
        })
    }

    deleteFollowUp(id) {
        let loading = this.loaderCtrl.create({
            content: 'Deleting, please wait...'
        });
        loading.present();
        this.apiService.deleteFollowUp(id).then(()=>{
            loading.dismiss();
            this.init();
        })
    }
}
