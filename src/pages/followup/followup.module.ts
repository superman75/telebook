import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FollowupPage } from './followup';
import {ComponentsModule} from '../../components/components.module'
@NgModule({
  declarations: [
    FollowupPage,
  ],
  imports: [
    IonicPageModule.forChild(FollowupPage),
      ComponentsModule
  ],
})
export class FollowupPageModule {}
