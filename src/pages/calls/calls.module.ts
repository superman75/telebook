import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CallsPage } from './calls';
import {ComponentsModule} from '../../components/components.module'
import {PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    CallsPage,
  ],
  imports: [
    IonicPageModule.forChild(CallsPage),
      ComponentsModule,
      PipesModule
  ],
})
export class CallsPageModule {}
