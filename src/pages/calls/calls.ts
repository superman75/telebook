import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import {ApiServiceProvider} from "../../providers/api-service/api-service";
import {CalldetailPage } from "../pages";
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-calls',
  templateUrl: 'calls.html',
})
export class CallsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private apiService: ApiServiceProvider,public alertCtrl: AlertController,
    private loaderCtrl: LoadingController) {
       this.getCall();
  }

//models
calls:any;
calls_pages:number;
avatar:any;
//function
goTocalldetail(call)
{
  this.navCtrl.push(CalldetailPage,{call: call});
}

getCall(){
  let loading = this.loaderCtrl.create({
  content: 'Please wait...'
  });
  loading.present();

  this.apiService.getCalls(1).then(data=>{
  this.calls = data["calls"];
  this.calls_pages = data["calls_page_quantity"];  
  loading.dismiss();
  })
}

doInfinite(infiniteScroll) {
  setTimeout(() => {
    if(this.calls_pages >= ((this.calls.length/10)+1))
    {
      this.apiService.getCalls((this.calls.length/10)+1).then(data=>{
        this.calls =  this.calls.concat(data["calls"]);
          infiniteScroll.complete();
      })
    }
  }, 500);
  
}

showConfirmBlackList() {
  const confirm = this.alertCtrl.create({
    title: 'Are you sure?',
    message: 'the contact will be added to the blacklist',
    buttons: [
      {
        text: 'Cancel',
        handler: () => {
          console.log('Disagree clicked');
        }
      },
      {
        text: 'OK',
        handler: () => {
          console.log('Agree clicked');
        }
      }
    ]
  });
  confirm.present();
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad CallsPage');
  }
  generateAvatar(name){
      return "https://ui-avatars.com/api/?name="+name+'&background=03dac6&color=fff&rounded=true';
  }
}
