import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgentsonlinePage } from './agentsonline';
import { PipesModule} from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    AgentsonlinePage,
  ],
  imports: [
    IonicPageModule.forChild(AgentsonlinePage),
      PipesModule
  ],
})
export class AgentsonlinePageModule {}
