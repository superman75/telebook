import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import {FirebaseServiceProvider} from "../../providers/firebase-service/firebase-service";
import {StorageServiceProvider} from "../../providers/storage-service/storage-service";

@IonicPage()
@Component({
  selector: 'page-agentsonline',
  templateUrl: 'agentsonline.html',
})
export class AgentsonlinePage {
time:any;
  constructor(private fBD: FirebaseServiceProvider, private storage: StorageServiceProvider) {
      this.currentData();
  }
  ionViewWillEnter(){
    this.getOnlineAgents();
  }
  online_agent:any = [];
  nowDate;
  myId=this.storage.user_info.user.id;
  getOnlineAgents() {
      this.fBD.getChat('online').subscribe(result => {
        setInterval(()=>{
            this.nowDate = Date.now();
        },1000);
        this.online_agent = result;
      })
  }
  currentData(){
    setInterval(()=>{
      this.time = Math.round(Date.now()/1000);
    },1000)
  }
}
