import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatePipe } from '@angular/common';
import {ToastController} from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { ApiServiceProvider } from "../../providers/api-service/api-service";
/**
 * Generated class for the AddReminderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for mo`re info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-reminder',
  templateUrl: 'add-reminder.html',
})
export class AddReminderPage {
  myDate:any;
  reminder:any;

  sender:any;
  sender_worker:any;
  message:any;
  date:any;
  key:any;
  worker_id:any;
  node:any;

  constructor(private apiService: ApiServiceProvider,public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams,private datePipe: DatePipe,private toastCtrl: ToastController) {
    this.sender = this.navParams.get('sender');
    this.sender_worker = this.navParams.get('sender_worker');
    this.message = this.navParams.get('message');
    this.date = this.navParams.get('date');
    this.key = this.navParams.get('key');
    this.worker_id = this.navParams.get('worker_id');
    this.node = this.navParams.get('node');
  }
  saveReminder(){
    if(!this.myDate){
     this.showToastError("Select a date, it is a required field");
    }else if(!this.reminder){
     this.showToastError("Type a message, it is a required field");
    }else{
      const loader = this.loadingCtrl.create({
        content: "Please wait...",
        duration: 0
      });
      loader.present();
      //console.log(this.node, this.key, this.myDate, this.reminder, this.sender, this.sender_worker, this.message, this.date,this.worker_id);
      this.apiService.setReminder(this.node, this.key, this.myDate, this.reminder, this.sender, this.sender_worker, this.message, this.date,this.worker_id).then(response=>{
        loader.dismiss();
        this.showToastSuccess(response[0]);
      }).catch(error=>{
          loader.dismiss();
        this.showToastError(error.error);
      });
    }
  }

  showToastSuccess(notification) {
    const toast = this.toastCtrl.create({
      message: notification,
      duration: 3000,
      cssClass: "bg-success",
      position: 'top',
    });
    toast.present();
  }

  showToastError(notification) {
    const toast = this.toastCtrl.create({
      message: notification,
      duration: 3000,
      cssClass: "bg-danger",
      position: 'top',
    });
    toast.present();
  }

}
