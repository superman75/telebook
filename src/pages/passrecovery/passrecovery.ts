import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup} from "@angular/forms";
import {ApiServiceProvider} from "../../providers/api-service/api-service";

/**
 * Generated class for the PassrecoveryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-passrecovery',
  templateUrl: 'passrecovery.html',
})
export class PassrecoveryPage {
  private form:FormGroup;
  validation_messages={
      'email':[
          {type: 'required', message: 'The email field is required'},
          {type: 'email', message: 'The email is invalid'}
      ],
  }
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private formBuilder: FormBuilder,
              private apiService: ApiServiceProvider,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController) {
    this.form = this.formBuilder.group({
        email: [null,[Validators.required, Validators.email]]
    })
  }
  email = null;
  submitForm(){
      let loading = this.loadingCtrl.create({
          content: 'Please wait...'
      });
      loading.present();
      this.apiService.recoverPassword({email: this.email}).then(data=>{
          loading.dismiss();
          this.toastCtrl.create({
              message: data['message'],
              cssClass: 'bg-secondary',
              duration: 5000
          }).present();
      },error=>{
          loading.dismiss();
          this.toastCtrl.create({
              message: error.error.message,
              position: 'top',
              cssClass: 'bg-danger',
              duration: 5000
          }).present();
      })
  }

}
