import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PassrecoveryPage } from './passrecovery';

@NgModule({
  declarations: [
    PassrecoveryPage,
  ],
  imports: [
    IonicPageModule.forChild(PassrecoveryPage),
  ],
})
export class PassrecoveryPageModule {}
