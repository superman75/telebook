import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {ApiServiceProvider} from "../../providers/api-service/api-service";
import {ChatPage} from "../pages";
import {ReminderPage} from "../pages";
import * as moment from 'moment'
import * as _ from "lodash";

@IonicPage()
@Component({
    selector: 'page-reminders',
    templateUrl: 'reminders.html',
})
export class RemindersPage {

    reminders: any;
    date: any = new Date();
    worker_id: any;
    token: any;
    node: any;

    internalReturn: any;
    internalKeys: any;
    did_number: any;
    person_name: any;

    externalReturn: any;
    externallKeys: any;
    external_conversation_id: any;
    priority: any;
    worker_person: any;
    customer_phone_number: any;

    reminders_count: any = [];
    reminderPage: any;
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private apiService: ApiServiceProvider,
                private loadingCtrl: LoadingController) {
        this.reminderPage = ReminderPage;
        this.init();
    }
    getCountDate() {
        this.reminders_count = [];
        for (let i = -3; i <= 3; i++){
            let type = i <= 0 ? 'subs' : 'sum';
            let date1 = this.returnDates(Math.abs(i),type,'MMMM DD');
            let date2 = this.returnDates(Math.abs(i),type,'YYYY-MM-DD');
            let count = _.size(_.filter(this.reminders,['datereminder',date2]));
           this.reminders_count.push({'date': date1,'date_reminder': date2,'count': count});
        }
    }
    getdataExternal_conversation() {
        this.apiService.getListMessage(this.worker_id).then((data) => {
            this.externalReturn = data;
            this.externallKeys = Object.keys(data);
            this.externallKeys.forEach(element => {
                if (this.externalReturn[element]["node"] == this.node) {
                    this.external_conversation_id = this.externalReturn[element]["external_conversation_id"];
                    this.priority = this.externalReturn[element]["priority"];
                    this.worker_person = this.externalReturn[element]["worker_person"];
                    this.customer_phone_number = this.externalReturn[element]["customer_phone_number"];
                }
            });
            this.navCtrl.push(ChatPage, {
                'customer_phone_number': this.customer_phone_number,
                'node': this.node,
                'external_conversation_id': this.external_conversation_id,
                'worker_id': this.worker_id,
                'priority_id': this.priority,
                'person_name': this.person_name,
                'did_number': this.did_number,
                'worker_person': this.worker_person
            })
        }, error => {
            console.log(error);
        });
    }

    init() {
        this.reminders = [];
        let loading = this.loadingCtrl.create({
            content: 'Loading, please wait...'
        }) ;
        loading.present();
        this.apiService.getReminders().then(response => {
            this.reminders = response["reminders"];
            this.getCountDate();
            loading.dismiss();
        }, error => {
            console.log(error);
        });
    }


    returnDates(day, type, format){
        if(type == 'sum'){
            return moment(new Date()).add(day,'days').format(format)
        }
        if(type == 'subs'){
            return moment(new Date()).subtract(day,'days').format(format)
        }
    }
}