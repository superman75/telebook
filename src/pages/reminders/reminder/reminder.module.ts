import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReminderPage } from './reminder';
import {ComponentsModule} from '../../../components/components.module'
@NgModule({
  declarations: [
    ReminderPage,
  ],
  imports: [
    IonicPageModule.forChild(ReminderPage),
      ComponentsModule
  ],
})
export class ReminderPageModule {}
