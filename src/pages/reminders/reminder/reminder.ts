import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {ChatPage} from "../../sms/inbox/chat/chat";
import {ApiServiceProvider} from "../../../providers/api-service/api-service";

@IonicPage()
@Component({
    selector: 'page-remainder',
    templateUrl: 'reminder.html',
})
export class ReminderPage {

    reminder_params_date: any;
    reminder_params_object: any;
    token: any;
    worker_id: any;
    node: any;
    internalReturn: any;
    internalKeys: any;
    externalKeys: any;
    did_number: any;
    person_name: any;
    externalReturn: any;
    priority: any;
    worker_person: any;
    customer_phone_number: any;
    external_conversation_id: any;
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private apiService: ApiServiceProvider,
                private toastCtrl: ToastController) {
        this.reminder_params_date = this.navParams.get('date');
        this.reminder_params_object = this.navParams.get('reminders');
    }

    goReminder(token, worker_id, node) {
        this.token = token;
        this.worker_id = worker_id;
        this.node = node;
        this.getdatainternal_conversation(this.worker_id);
        this.getdataExternal_conversation();
    }

    deleteReminder(id, e){
        this.apiService.deleteReminder(id).then(response=>{
            let toast = this.toastCtrl.create({
                message: response["message"],
                duration: 3000,
                position: 'top',
                cssClass:"bg-success",
            });
            toast.present();
            e.path[3].remove();
        })
    }

    getdatainternal_conversation(worker_id) {
        this.apiService.getUserMessenger().then((data) => {
            this.internalReturn = data;
            this.internalKeys = Object.keys(data);
            this.internalKeys.forEach(element => {
                if (this.internalReturn[element]["worker_id"] == worker_id) {
                    this.did_number = this.internalReturn[element]["did_number"];
                    this.person_name = this.internalReturn[element]["person_name"];
                }
            });
        }, error => {
            console.log(error);
        });
    }

    getdataExternal_conversation() {
        this.apiService.getListMessage(this.worker_id).then((data) => {
            this.externalReturn = data;
            this.externalKeys = Object.keys(data);
            this.externalKeys.forEach(element => {
                if (this.externalReturn[element]["node"] == this.node) {
                    this.external_conversation_id = this.externalReturn[element]["external_conversation_id"];
                    this.priority = this.externalReturn[element]["priority"];
                    this.worker_person = this.externalReturn[element]["worker_person"];
                    this.customer_phone_number = this.externalReturn[element]["customer_phone_number"];
                }
            });
            this.navCtrl.push(ChatPage, {
                'customer_phone_number': this.customer_phone_number,
                'node': this.node,
                'external_conversation_id': this.external_conversation_id,
                'worker_id': this.worker_id,
                'priority_id': this.priority,
                'person_name': this.person_name,
                'did_number': this.did_number,
                'worker_person': this.worker_person,
                'origin': this.token
            })
        }, error => {
            console.log(error);
        });
    }
}
