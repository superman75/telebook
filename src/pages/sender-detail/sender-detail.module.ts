import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SenderDetailPage } from './sender-detail';
import {ComponentsModule} from '../../components/components.module'

@NgModule({
  declarations: [
    SenderDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SenderDetailPage),
      ComponentsModule
  ],
})
export class SenderDetailPageModule {}
