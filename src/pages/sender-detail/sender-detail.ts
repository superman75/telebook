import {Component, ViewChild} from '@angular/core';
import {Content, IonicPage, NavController, NavParams} from 'ionic-angular';
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import {FirebaseServiceProvider} from "../../providers/firebase-service/firebase-service";

/**
 * Generated class for the SenderDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sender-detail',
  templateUrl: 'sender-detail.html',
})
export class SenderDetailPage {
    @ViewChild(Content) content: Content;
  image: any;
  external_conversation_id:any;
  sender:any="";
  person_name:string="";
  did_number:any="";

  name:string="";
  last_name:string="";
  username:string="";
  email:string="";
  role:string="";
  did:any="";
  backup_email:string="";
  phone_number:any="";
  address:string="";
  filename:string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
        private apiService: ApiServiceProvider,
              private fbServices: FirebaseServiceProvider)
    {
      this.external_conversation_id = this.navParams.get('external_conversation_id');
      this.sender = this.navParams.get('sender');
      this.person_name = this.navParams.get('person_name');
      this.did_number = this.navParams.get('did_number');
       this.getdata(this.external_conversation_id,this.sender)
  }
  getdata(external_conversation_id,sender){
     this.apiService.getSenderDetaillMessage(external_conversation_id).then(response=>{
      if(response){
        this.name=response["workers"][sender]["name"];
        this.last_name=response["workers"][sender]["last_name"];
        this.username=response["workers"][sender]["username"];
        this.email=response["workers"][sender]["email"];
        this.role=response["workers"][sender]["role"];
        this.did=response["workers"][sender]["did"];
        this.phone_number=response["workers"][sender]["phone_number"];
        this.address=response["workers"][sender]["address"];
        this.getProfileImageUrl(response["workers"][sender]["profile_image"]);
        console.log(this.image);
      }
   },error=>{
      console.log(error);
   })
  }
    getProfileImageUrl(name){
      this.fbServices.getProfileUrl(name).then(response=>{
            this.image = response;
        });
    }
}
