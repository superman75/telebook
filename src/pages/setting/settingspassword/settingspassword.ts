

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ApiServiceProvider} from "../../../providers/api-service/api-service";
import { AlertController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import { ToastController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-settingspassword',
  templateUrl: 'settingspassword.html',
})

export class SettingspasswordPage {

  private form:FormGroup;
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     private apiService: ApiServiceProvider,
     public alertCtrl: AlertController,
     private formBuilder: FormBuilder,
     public toastCtrl: ToastController) 
     {
      this.form = this.formBuilder.group({
        currentPassword: [null,[Validators.required,Validators.required]],
        newPassword: [null,[Validators.required, Validators.minLength(6)]],
        confirmPassword: [null,[Validators.required, Validators.minLength(6)]],

    })
  }

   //validaciones
   validation_messages ={
    'currentPassword':[
        {type: 'required', message: 'The current password is required'},
    ],
    'newPassword':[
        {type: 'required', message: 'The password field is required'},
        {type: 'minlength', message: 'The password field is very short'}
    ],
    'confirmPassword':[
      {type: 'required', message: 'the password confirmation is not valid'}
  ]
  }


  //variables
  image="http://i.pravatar.cc/100";
  currentPassword:null;
  newPassword:null;
  confirmPassword:null;

  badCurrentPassword="";
  badPassword="";
  badNewPassword="";
  badConfirmPassword="";


  //modelos
    credentials:any={};
    response:any={};

    submitForm(){
      this.credentials.current_password=this.currentPassword;
      this.credentials.password=this.newPassword;
      this.credentials.password_confirmation=this.confirmPassword;
      this.apiService.updatePassword(this.credentials).then(data=>{
        if(data==null){
          this.showToastSuccess("Credentials update success");
          this.currentPassword=null;
          this.newPassword=null;
          this.confirmPassword=null;
        }
        console.log(data);
      },error=>{
        if(error.current_password){
            this.badCurrentPassword=error.current_password[0];
        }
        if(error.password){
            this.badPassword=error.password[0];
        }
        if(error.password_confirmation[0]){
          this.badNewPassword=error.password_confirmation[0];
        }if(error.password_confirmation[1]){
          this.badNewPassword=error.password_confirmation[1];
        }
        this.showToast();
      });
    }

    showToast() {
      const toast = this.toastCtrl.create({
        message:this.badCurrentPassword
         +'\n' + this.badPassword 
        +'\n' + this.badNewPassword 
        + '\n' +this.badConfirmPassword 
        + '\n',
        duration: 5000,
        cssClass:"bg-danger",
        position: 'top',
      });
      toast.present();
    }
    
    showToastSuccess(text) {
      const toast = this.toastCtrl.create({
        message:text,
        duration: 5000,
        cssClass:"bg-success",
        position: 'top',
      });
      toast.present();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingspasswordPage');
  }
}
