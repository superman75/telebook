import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingspasswordPage } from './settingspassword';
import {ComponentsModule} from '../../../components/components.module'

@NgModule({
  declarations: [
    SettingspasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(SettingspasswordPage),
      ComponentsModule
  ],
})
export class SettingspasswordPageModule {}
