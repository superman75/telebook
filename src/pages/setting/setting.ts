import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, ToastController} from 'ionic-angular';
import {ApiServiceProvider} from "../../providers/api-service/api-service";
import {StorageServiceProvider} from "../../providers/storage-service/storage-service";
import {FirebaseServiceProvider} from "../../providers/firebase-service/firebase-service";
import {NativeProvider} from "../../providers/native/native";

@IonicPage()
@Component({
    selector: 'page-setting',
    templateUrl: 'setting.html',
})
export class SettingPage {

    constructor(private alertCtrl: AlertController,
                private apiService: ApiServiceProvider,
                private storageCtrl: StorageServiceProvider,
                public toastCtrl: ToastController,
                public loadingCtrl: LoadingController,
                private uploadService: FirebaseServiceProvider, private nativeCtrl: NativeProvider) {
        this.mai();
    }

    //VARIABLES
    reg: RegExp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    regPhone: RegExp = /^\+([0-9]{11,15})$/;

    //MODELS
    user: any = [];
    role: any = [];
    DIDS: any = [];
    cities: any = [];
    data: any = [];
    idRol: any;
    states:any;
    profile_image: any = {url: '', name: ''};
    paramsRequestUser: any = {};

    mai() {
        const loader = this.loadingCtrl.create({
            content: "Please wait...",
        });
        loader.present();
        this.apiService.getUserData().then(response => {
            if (response) {
                this.states=response["states"];
                this.user = response["users"];
                this.role = response["roles"];
                this.DIDS = response["dids"];
                this.profile_image.name = response["profile_image"];
                if(response["profile_image_url"]!=null){
                    this.profile_image.url = response["profile_image_url"]; 
                }
                Object.keys(this.user).forEach((key) => {
                    this.apiService.addUrlProfilePhoto(this.user[key]);
                    this.getCities(this.user[key]["state_id"], null);
                    this.idRol = this.user[key]["role_id"];
                });
                this.setData();
                loader.dismiss();
            }
        }, error => {
            console.log(error);
        });
    }

    setData() {
        if (this.user) {
            Object.keys(this.user).forEach((key) => {
                this.paramsRequestUser.address = this.user[key]["address"];
                this.paramsRequestUser.backup_email = this.user[key]["backup_email"];
                this.paramsRequestUser.city_id = this.user[key]["city_id"];
                this.paramsRequestUser.did_id = this.user[key]["did_id"];
                this.paramsRequestUser.email = this.user[key]["email"];
                this.paramsRequestUser.last_name = this.user[key]["last_name"];
                this.paramsRequestUser.name = this.user[key]["name"];
                this.paramsRequestUser.phone_number = this.user[key]["phone_number"];
                this.paramsRequestUser.role_id = this.user[key]["role_id"];
                this.paramsRequestUser.state_id = this.user[key]["state_id"];
                this.paramsRequestUser.user_id = this.storageCtrl.user_info.user.id;
                this.profile_image.name = this.user[key]["profile_image"];
                if(this.user[key]["profile_image_url"]!=null){
                    this.profile_image.url = this.user[key]["profile_image_url"]; 
                }
                
            });
            Object.keys(this.user).forEach((key) => {
                this.data.state_name = this.user[key]["state"];
                this.data.city_name = this.user[key]["city"];
                this.data.rol_name = this.user[key]["role"];
                this.data.did_number = this.user[key]["did"];
            });
        }
    }

    name(value) {
        let name = this.alertCtrl.create({
            title: 'First Name',
            inputs: [
                {
                    name: 'name',
                    placeholder: 'Change first name',
                    value: value,
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                },
                {
                    text: 'Save',
                    handler: data => {
                        if (data.name == "") {
                            this.showToast("first name is required");
                            this.name(data.name);
                        } else {
                            this.user.name = data.name;
                            this.paramsRequestUser.name = data.name;
                        }
                    }
                }
            ]
        });
        name.present();
    };

    last_name(value) {
        let last_name = this.alertCtrl.create({
            title: 'Last Name',
            inputs: [
                {
                    name: 'LastName',
                    placeholder: 'Change last name',
                    value: value,
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                },
                {
                    text: 'Save',
                    handler: data => {
                        if (data.LastName == "") {
                            this.showToast("last name is required");
                            this.last_name(data.LastName);
                        } else {
                            this.user.last_name = data.LastName;
                            this.paramsRequestUser.last_name = data.LastName;
                        }
                    }
                }
            ]
        });
        last_name.present();
    };

    address(value) {
        let address = this.alertCtrl.create({
            title: 'Change address',
            inputs: [
                {
                    name: 'Address',
                    placeholder: 'Change address',
                    value: value,
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                },
                {
                    text: 'Save',
                    handler: data => {
                        if (data.Address == "") {
                            this.showToast("Address is required");
                            this.address(data.Address);
                        } else {
                            this.user.address = data.Address;
                            this.paramsRequestUser.address = data.Address;
                        }
                    }
                }
            ]
        });
        address.present();
    };

    email(value) {
        let email = this.alertCtrl.create({
            title: 'Change email',
            inputs: [
                {
                    name: 'email',
                    placeholder: 'example@example.com',
                    value: value,
                    type: "email",
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                },
                {
                    text: 'Save',
                    handler: data => {
                        if (data.email == "" || !this.reg.test(data.email)) {
                            this.showToast("Invalid email");
                            this.email(data.email);
                        } else {
                            this.user.email = data.email;
                            this.paramsRequestUser.email = data.email;
                        }
                    }
                }
            ]
        });
        email.present();
    };

    rol(value) {
        if (this.idRol == 1 || this.idRol == 2) {
            let alert = this.alertCtrl.create();
            alert.setTitle('Change rol');

            Object.keys(this.role).forEach((key) => {
                if (key == value) {
                    alert.addInput({
                        type: 'radio',
                        label: this.role[key],
                        checked: true,
                        value: key
                    });
                } else {
                    alert.addInput({
                        type: 'radio',
                        label: this.role[key],
                        checked: false,
                        value: key
                    });
                }
            })
            alert.addButton('Cancel');
            alert.addButton({
                text: 'OK',
                handler: data => {
                    Object.keys(this.role).forEach((key) => {
                        if (data == key) {
                            this.paramsRequestUser.role_id = data;
                            this.data.rol_name = this.role[key];
                            this.paramsRequestUser.role_id = data;
                        }
                    })
                }
            });
            alert.present();
        } else {
            this.showToast("You do not have permission to change the role");
        }
    }

    dids(value) {
        let alert = this.alertCtrl.create();
        alert.setTitle('Change DIDS');

        Object.keys(this.DIDS).forEach((key) => {
            if (key == value) {
                alert.addInput({
                    type: 'radio',
                    label: this.DIDS[key].number + (this.DIDS[key].provider == "1" ? " (Twilio)" : "(Bandwidth)"),
                    checked: true,
                    value: key
                });
            } else {
                alert.addInput({
                    type: 'radio',
                    label: this.DIDS[key].number + (this.DIDS[key].provider == "1" ? " (Twilio)" : "(Bandwidth)"),
                    checked: false,
                    value: key
                });
            }
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {
                Object.keys(this.DIDS).forEach((key) => {
                    if (data == key) {
                        this.user.did_id = data;
                        this.data.did_number = this.DIDS[key];
                        this.paramsRequestUser.did_id = data;
                    }
                })
            }
        });
        alert.present();
    }

    phone_number(value) {
        let phone_number = this.alertCtrl.create({
            title: 'Phone number',
            inputs: [
                {
                    name: 'phone_number',
                    placeholder: 'Example +50512124512',
                    type: "tel",
                    value: value,
                    max: 12,
                    min: 8,
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                },
                {
                    text: 'Save',
                    handler: data => {
                        if (data.phone_number == "" || !this.regPhone.test(data.phone_number)) {
                            this.showToast("phone number is invalid, Example +50512124512");
                            this.phone_number(data.phone_number);
                        } else {
                            this.user.phone_number = data.phone_number;
                            this.paramsRequestUser.phone_number = data.phone_number;
                        }
                    }
                }
            ]
        });
        phone_number.present();
    };

    contact_email(value) {
        let contact_email = this.alertCtrl.create({
            title: 'Contact email',
            inputs: [
                {
                    name: 'contact_email',
                    placeholder: 'example@example.com',
                    value: value,
                    type: "email",
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                },
                {
                    text: 'Save',
                    handler: data => {
                        if (data.contact_email == "" || !this.reg.test(data.contact_email)) {
                            this.showToast("invalid email");
                            this.contact_email(data.contact_email);
                        } else {
                            this.user.backup_email = data.contact_email;
                            this.paramsRequestUser.backup_email = data.contact_email;
                        }
                    }
                }
            ]
        });
        contact_email.present();
    };

    state(value) {  
        let click = 1;
        let alert = this.alertCtrl.create();
        if (this.states) {
            alert.setTitle('Change state');
        } else {
            alert.setTitle('No data to display');
        }

        if (this.states) {
            Object.keys(this.states).forEach((key) => {
                if (key == value) {
                    alert.addInput({
                        type: 'radio',
                        label: this.states[key],
                        checked: true,
                        value: key
                    });
                } else {
                    alert.addInput({
                        type: 'radio',
                        label: this.states[key],
                        checked: false,
                        value: key
                    });
                }
            })
        }
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {
                if (this.states) {
                    Object.keys(this.states).forEach((key) => {
                        if (data == key) {
                            this.paramsRequestUser.state_id = data;
                            this.data.state_name = this.states[key];
                            this.data.city_name = "Select a city";
                            this.paramsRequestUser.city_id = null;
                        }
                    })
                }
                this.getCities(data, click)
            }
        });
        alert.present();
    }

    city(value) {
       let alert = this.alertCtrl.create();
        let loading = this.loadingCtrl.create({
            content: "Loading cities, please wait..."
        });
        if (this.cities) {
            alert.setTitle('Change city');
        } else {
            alert.setTitle('No data to display');
        }
        if (this.cities) {
            const start = async () => {
                await loading.present();
                this.cities.forEach(element => {
                    if (element["id"] == value) {
                        alert.addInput({
                            type: 'radio',
                            label: element["name"],
                            checked: true,
                            value: element["id"],
                        });
                    } else {
                        alert.addInput({
                            type: 'radio',
                            label: element["name"],
                            checked: false,
                            value: element["id"],
                        });
                    }
                });
                alert.present();
            };
            start();
        }
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {
                if (this.cities) {
                    this.cities.forEach(element => {
                       if (data == element["id"]) {
                            this.data.city_name = element["name"];
                            this.paramsRequestUser.city_id = data;
                        }
                    });
                }
            }
        });
        loading.dismiss();
    }

    getCities(id_state, click) {
        console.log(id_state);
        if (click) {
            const loader = this.loadingCtrl.create({
                content: "Please wait...",
            });
            loader.present();
            this.apiService.getCytiByState(id_state).then((data) => {
                this.cities = data["Cities_filter"];
                console.log("cities =>",this.cities);
                loader.dismiss();
            });
        } else {
            this.apiService.getCytiByState(id_state).then((data) => {
                this.cities = data["Cities_filter"];
                console.log("cities =>",this.cities);
            });
        }
    }

    updateUser() {
        const loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 0
        });
        loader.present();
        this.apiService.updateDataUser(this.paramsRequestUser).then(data => {
            loader.dismiss();
            setTimeout(() => {
                this.showToastSuccess("Successfully updated data");
            }, 1000);
        }, error => {
            console.log(error.error);
            this.showToast("An unexpected error has occurred, try again");
            loader.dismiss();
        });
    }

    showToast(notification) {
        const toast = this.toastCtrl.create({
            message: notification,
            duration: 3000,
            cssClass: "bg-danger",
            position: 'top',
        });
        toast.present();
    }

    showToastSuccess(notification) {
        const toast = this.toastCtrl.create({
            message: notification,
            duration: 3000,
            cssClass: "bg-success",
            position: 'top',
        });
        toast.present();
    }

    uploadProfileImage(old_name) {
        this.nativeCtrl.findImageFromPhone().then(data => {
            const loader = this.loadingCtrl.create({
                content: "Uploading file, please wait...",
            });
            loader.present();
            let name = Date.now();
            if (old_name != null) {
                this.uploadService.deleteImage(old_name);
            }
            this.uploadService.uploadFileNew(data["image"], name, 'profile-image/').then(result => {
                this.apiService.updateProfile(name, result['url']);
                this.profile_image.url = result["url"];
                loader.dismiss();
            })
        });
    }
}