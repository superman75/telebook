import { Component } from '@angular/core';
import {IonicPage} from 'ionic-angular';
import { GalleryPhotoPage} from "../../pages";

@IonicPage()
@Component({
  selector: 'page-configuration',
  templateUrl: 'configuration.html',
})
export class ConfigurationPage {
    gallery:any;
  constructor() {
      this.gallery = GalleryPhotoPage;
  }

}
