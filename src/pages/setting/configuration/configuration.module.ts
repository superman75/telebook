import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfigurationPage } from './configuration';
import {ComponentsModule} from '../../../components/components.module'
@NgModule({
  declarations: [
    ConfigurationPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfigurationPage),
      ComponentsModule
  ],
})
export class ConfigurationPageModule {}
