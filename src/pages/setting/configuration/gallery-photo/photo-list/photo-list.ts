import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavParams, ViewController} from 'ionic-angular';
import {FirebaseServiceProvider} from "../../../../../providers/firebase-service/firebase-service";
import {isUndefined} from "ionic-angular/util/util";

@IonicPage()
@Component({
  selector: 'page-photo-list',
  templateUrl: 'photo-list.html',
})
export class PhotoListPage {
  id:any;
  images:any=[];
  chat:boolean;
  constructor(public navParams: NavParams,
              private fireDB: FirebaseServiceProvider,
              private viewCtrl: ViewController, private loaderCtrl: LoadingController) {
    this.id = navParams.get('id');
    this.chat = navParams.get('chat');
    this.fireDB.getChat('gallery/'+this.id).subscribe(data=>{
      this.images = data;
    });
  }
  dismiss(url = null){
    if(!isUndefined(this.chat)){
      this.viewCtrl.dismiss(url);
    }
  }
  openGallery(){
    this.fireDB.openGallery().then(result=>{
      this.fireDB.convertImage(result).then(result=>{
        let loader = this.loaderCtrl.create({
            content: 'Uploading image, please wait...'
        });
        loader.present();
          this.fireDB.uploadFileNew(result).then(result=>{
              this.fireDB.pushNode('gallery/'+this.id,{date: result["name"], url: result["url"]});
              loader.dismiss();
          })
      })
    })
  }
}
