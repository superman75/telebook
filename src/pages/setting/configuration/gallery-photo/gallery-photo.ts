import { Component } from '@angular/core';
import {IonicPage, LoadingController} from 'ionic-angular';
import {ApiServiceProvider} from "../../../../providers/api-service/api-service";
import {PhotoListPage} from "../../../pages";

@IonicPage()
@Component({
  selector: 'page-gallery-photo',
  templateUrl: 'gallery-photo.html',
})
export class GalleryPhotoPage {

  users:any=[];
  loading:any;
  photoList:any;
  constructor(private apiServices: ApiServiceProvider, loadingCtrl: LoadingController) {
      this.photoList = PhotoListPage
    this.loading = loadingCtrl.create({
        content: 'Please wait...'
    });
    this.loading.present();
    this.apiServices.getAllUsers().then(data=>{
      this.users = data['users'];
      this.loading.dismiss();
    });
      console.log(this.users);
  }
  getAvatarUser(username){
    return this.apiServices.getProfilePhoto(encodeURI(username));
  }
}
