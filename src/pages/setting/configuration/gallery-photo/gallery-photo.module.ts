import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GalleryPhotoPage } from './gallery-photo';
import {ComponentsModule} from '../../../../components/components.module'
@NgModule({
  declarations: [
    GalleryPhotoPage,
  ],
  imports: [
    IonicPageModule.forChild(GalleryPhotoPage),
      ComponentsModule
  ],
})
export class GalleryPhotoPageModule {}
