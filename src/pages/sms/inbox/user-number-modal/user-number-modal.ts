import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {ApiServiceProvider} from "../../../../providers/api-service/api-service";

@IonicPage()
@Component({
  selector: 'page-user-number-modal',
  templateUrl: 'user-number-modal.html',
})
export class UserNumberModalPage {

  constructor(
      public view: ViewController,
      private apiService: ApiServiceProvider,
      private navParams: NavParams,
      private toastCtrl: ToastController,
      private loaderCtrl: LoadingController) {
    this.worker_id = this.navParams.get('worker_id');
  }
  worker_id:number;
  numbers:any = {
    customers: null
  };
  number_searched:number = null;
  close(params:any = null){
    if(params==null){
        this.view.dismiss({params: false})
    }else{
        let pattern = /\+[1]\d{10}/g;
        if(pattern.test(params.phone_number)){
            let loader = this.loaderCtrl.create({
                content: "Please wait..." ,
                dismissOnPageChange: true
            });
            loader.present();
            this.apiService.getChatFromNumberFound(params.phone_number, params.id, this.worker_id).then(data=>{
                this.view.dismiss({params: true,data: data})
            },error=>{
                let toast_option = {
                    message:error.error.phone_number,
                    position: 'top',
                    duration: 5000,
                    cssClass: 'bg-danger'
                };
                loader.dismiss();
                this.toastCtrl.create(toast_option).present();
            })
        }else{
            let toast_option = {
                message:'Format number is invalid. Ex. +14158586273',
                position: 'top',
                duration: 5000,
                cssClass: 'bg-danger'
            };
            this.toastCtrl.create(toast_option).present();
        }
    }
  }
  getItem(va){
      const val = va.target.value;
      this.apiService.getNumberFinder(val).then(data=>{
        if(data !=null){
            this.numbers = data;
        }else{
            this.number_searched = val;
          this.numbers.customers = [];
        }
      })
  };
}
