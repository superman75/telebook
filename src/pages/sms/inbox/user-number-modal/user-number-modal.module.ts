import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserNumberModalPage } from './user-number-modal';
import {ComponentsModule} from '../../../../components/components.module'

@NgModule({
  declarations: [
    UserNumberModalPage,
  ],
  imports: [
    IonicPageModule.forChild(UserNumberModalPage),
      ComponentsModule
  ],
})
export class UserNumberModalPageModule {}
