import {Component, ViewChild} from '@angular/core';
import {
    ActionSheetController,
    AlertController,
    Content,
    IonicPage,
    LoadingController,
    ModalController,
    NavController,
    NavParams,
    ToastController
} from 'ionic-angular';
import {FirebaseServiceProvider} from "../../../../providers/firebase-service/firebase-service";
import {ApiServiceProvider} from "../../../../providers/api-service/api-service";
import {StorageServiceProvider} from "../../../../providers/storage-service/storage-service";
import {ImagePicker} from "@ionic-native/image-picker";
import {
    ImageGalleryPreviewPage,
    AddReminderPage,
    SenderDetailPage,
    PhotoListPage
} from "../../../pages";
import {isDefined, isUndefined} from "ionic-angular/util/util";
import * as _ from 'lodash';

@IonicPage()
@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html',
})
export class ChatPage {
    @ViewChild(Content) content: Content;
    title: any;
    node: any = null;
    chat_message: any = [];
    message_written: any = '';
    external_conversation_id: number;
    internal_conversation_id: number;
    person_name: string;
    worker_person: string;
    customer_id: number;
    worker_id: number;
    did_number: any;
    mms: boolean = true;
    quicklyanswers: any;
    myTypingId: any;
    userTyping: any = [];
    myName: any;
    direct_count: any;
    myId: any;
    origin: any;
    autoScroll: any;
    scrollContentBottom: any;
    scrollContentTop: any;
    customer_phone_number: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private loadingCtrl: LoadingController,
                private firebaseDB: FirebaseServiceProvider,
                private apiService: ApiServiceProvider,
                private modalCtrl: ModalController,
                private userInfo_external_conversation: StorageServiceProvider,
                private imageCtrl: ImagePicker,
                private actionCtrl: ActionSheetController,
                private toastCtrl: ToastController,
                private alertCtrl: AlertController,
                private storageCtrl: StorageServiceProvider) {
        this.myId = this.storageCtrl.user_info.user.id;
        const loading = this.loadingCtrl.create({
            content: "Please wait...",
        });
        loading.present();
        this.userInfo_external_conversation._priority = this.navParams.get('priority_id');
        this.node = this.navParams.get('node');
        this.external_conversation_id = this.navParams.get('external_conversation_id');
        this.internal_conversation_id = this.navParams.get('internal_conversation_id');
        this.did_number = this.navParams.get('did_number');
        this.worker_id = this.navParams.get('worker_id');
        this.person_name = this.navParams.get('person_name');
        this.worker_person = this.navParams.get('worker_person');
        this.customer_id = this.navParams.get('customer_id');
        this.direct_count = this.navParams.get('direct_count');
        this.origin = this.navParams.get('origin');
        this.myName = this.storageCtrl.user_info.user.name;
        this.scrollContentBottom = this.navParams.get('scroll-content-bottom');
        this.scrollContentTop = this.navParams.get('scroll-content-top');
        console.log(this.customer_phone_number);
        this.customer_phone_number = this.navParams.get('customer_phone_number');
        if (isDefined(this.navParams.get('customer_phone_number'))) {
            this.title = this.navParams.get('customer_phone_number');
        } else {
            this.title = this.navParams.get('person_name');
            this.mms = false;
        }
        this.delete_message(this.node);
        this.firebaseDB.getChat4(this.node).subscribe(data => {
            this.chat_message = [];
            data.forEach(data => {
                this.chat_message.push(_.assign(data.payload.val(), {"token": data.key}));
            });
            this.userInfo_external_conversation.external_conversation = this.external_conversation_id;
        });
        loading.dismiss();
        this.getquicklymessage();
        this.checkTyping();
        this.firebaseDB.getTyping(this.node).subscribe(data => {
            console.log(this.node, data);
            this.userTyping = data;
        })
    }

    getquicklymessage() {
        this.apiService.getMessagepredefinid().then((data) => {
            this.quicklyanswers = data['answers'];
        }, error => {
            console.log(error);
        });
    }

    //METODOS
    sendMessageWritten(imageURL, message) {
        let identi;
        if (isDefined(this.external_conversation_id)) {
            identi = this.external_conversation_id;
        } else {
            identi = this.internal_conversation_id;
        }
        let data = {
            id: identi,
            imageURL: imageURL,
            message: message,
            type: null
        };
        if (isDefined(this.internal_conversation_id)) {
            this.apiService.sendMessageChat(data);
        } else {
            this.apiService.sendMessage(data);
        }
        this.message_written = '';
        setTimeout(() => {
            this.content.scrollToBottom(200);
        }, 1100);
    }

    sendDefaultMessage() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Quickly Answers');
        if (this.quicklyanswers) {
            Object.keys(this.quicklyanswers).forEach((key) => {
                alert.addInput({
                    type: 'radio',
                    label: this.quicklyanswers[key]['answer'],
                    checked: false,
                    value: this.quicklyanswers[key]['answer']
                });
            })
        } else {
            alert.setSubTitle("Quick answers not available")
        }
        alert.addButton('Cancel');
        alert.addButton({
            text: 'Send',
            handler: data => {
                if (data) {
                    this.sendMessageWritten(null, data);
                }
            }
        });
        alert.present();
    }

    ionViewDidLoad() {
        let name = this.navCtrl.getActive();
        if (name.component.name == 'ChatPage') {
            if(this.scrollContentBottom != undefined){
                document.querySelector('page-chat .scroll-content').classList.add('scroll-content-bottom');
            } else {
                document.querySelector('page-chat .scroll-content').classList.remove('scroll-content-bottom');
            }
            if(this.scrollContentTop != undefined){
                document.querySelector('page-chat .scroll-content').classList.add('scroll-content-top');
            } else {
                document.querySelector('page-chat .scroll-content').classList.remove('scroll-content-top');
            }
            if (isDefined(this.origin)) {
                setTimeout(() => {
                    this.autoScroll = document.getElementById(this.origin);
                    this.autoScroll.scrollIntoView();
                    this.autoScroll.classList.add('pulse-followup');
                }, 1100);
            } else {
                setTimeout(() => {
                    this.content.scrollToBottom(200);
                    this.delete_message(this.node)
                }, 1100);
            }
        }
    }

    ionViewDidEnter() {
        if (this.did_number == null) {
            this.toastCtrl.create({
                message: "You can't send message until you assign a Did to this User",
                position: 'bottom',
                cssClass: 'bg-danger',
                dismissOnPageChange: true,
                showCloseButton: true
            }).present();
        }
    }

    findImageFromGallery() {
        let imageGallery = this.modalCtrl.create(PhotoListPage, {id: this.worker_id, chat: true});
        imageGallery.onDidDismiss(data => {
            if (data != null) {
                this.openPreview('data:image/jpeg;base64,' + data, 'gallery', data)
            }
        });
        imageGallery.present();
    }

    findImageFromPhone() {
        this.imageCtrl.hasReadPermission().then((result) => {
            if (result) {
                this.openGallery();
            } else {
                this.imageCtrl.requestReadPermission().then(() => {
                    this.openGallery();
                })
            }
        });
    }

    findImage() {
        let actionSheet = this.actionCtrl.create({
            title: 'Choose your source',
            buttons: [
                {
                    text: 'Telabook Gallery',
                    icon: 'cloud-download',
                    handler: () => {
                        this.findImageFromGallery();
                    }
                },
                {
                    text: 'My Phone Images',
                    icon: 'cloud-upload',
                    handler: () => {
                        this.findImageFromPhone();
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                }
            ]
        });
        actionSheet.present();
    }

    openPreview(image64, origin, image) {
        let preview = this.modalCtrl.create(ImageGalleryPreviewPage, {image64: image64, origin: origin, image: image});
        preview.present();
        preview.onDidDismiss(data => {
            if (data.params) {
                this.sendMessageWritten(data["image"], data["message"]);
            }
        })
    }

    delete_message(node) {
        if (this.direct_count > 0 || isUndefined(this.direct_count)) {
            this.firebaseDB.deleteMessage(node);
            this.direct_count = 0;
        }
    }

    openGallery() {
        this.firebaseDB.openGallery().then(result => {
            this.firebaseDB.convertImage(result).then(result => {
                this.openPreview('data:image/jpeg;base64,' + result, 'phone', result)
            })
        });
    }

    scrollBotton() {
        this.content.scrollToBottom(0);
    }

    checkTyping() {
        this.firebaseDB.checkTyping(this.node, this.myName).then(data => {
            this.myTypingId = data; //if null, I never have written something in this chat, return firebase ID
        });
    }

    isTyping(val) {
        let isWriting: boolean;
        val == '' ? isWriting = false : isWriting = true;
        this.firebaseDB.IsTyping(this.node, this.myTypingId, isWriting);
        setTimeout(()=>{
            this.firebaseDB.IsTyping(this.node, this.myTypingId, false);
        },20000)
    }

    showOption(sender, sender_is_worker, message, date, key, tags, type) {
        let alert = this.alertCtrl.create();
        alert.setTitle('Please select an option');
        if(type == 'worker'){
            alert.addInput({
                type: 'radio',
                label: 'Sender Details',
                checked: false,
                value: '1'
            });
        }
        alert.addInput({
            type: 'radio',
            label: 'Mark as FollowUp',
            checked: false,
            value: '2'
        });
        alert.addInput({
            type: 'radio',
            label: 'Set Reminder',
            checked: false,
            value: '3',
        });
        alert.addInput({
            type: 'radio',
            label: 'See tags',
            checked: false,
            value: '4',
        });
        alert.addInput({
            type: 'radio',
            label: 'Set new Tag',
            checked: false,
            value: '5',
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'Ok',
            handler: data => {
                switch (data) {
                    case "1":
                        this.navCtrl.push(SenderDetailPage, {
                            "external_conversation_id": this.external_conversation_id,
                            "sender": sender,
                            "person_name": this.person_name,
                            "did_number": this.did_number
                        });
                    break;
                    case "2":
                        this.markasFollowUp(sender, sender_is_worker, message, date, key);
                        break;
                    case "3":
                        this.addReminder(sender, sender_is_worker, message, date, key);
                        break;
                    case "4":
                        alert.dismiss();
                        this.seeTags(tags,key);
                        return false;
                    case "5":
                        alert.dismiss();
                        this.setTags(tags,key);
                        return false;
                    default:
                        break;
                }
            }
        });
        alert.present();
    }

    seeTags(tags,key){
        let alertTag;
        if(isDefined(tags)){
            alertTag = this.alertCtrl.create();
            alertTag.setTitle('Tags');
            let alltag = tags.split(',');
            alltag.forEach(data => {
                alertTag.addInput({
                    type: 'checkbox',
                    label: data,
                    checked: false,
                    value: data,
                });
            });
            alertTag.addButton({
                text: 'Remove',
                handler: data => {
                    if(isDefined(data)){
                        let tagsArray = tags.split(',');
                        let newTags = _.pullAll(tagsArray,data);
                        this.firebaseDB.updateTag(this.node,key,newTags.length > 0 ? newTags.toString() : null);
                    }
                }
            });
            alertTag.addButton({
                text: 'Ok',
            });
        } else {
            alertTag = this.alertCtrl.create({
                title: "Tags",
                subTitle: "No tags",
                buttons: ["OK"]
            })
        }
        alertTag.present();
    }

    setTags(tags,key){
        let alertTagSet = this.alertCtrl.create({
            title: "New tag",
            message: "Add your tags's description, you can add multiple tags separating it by comma.",
            inputs: [
                {
                    name: 'tag',
                    placeholder: "Tag's Description"
                },
            ],
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: "Save",
                    handler: data => {
                        if(isUndefined(tags)){
                            this.firebaseDB.updateTag(this.node,key,data.tag);
                        } else {
                            this.firebaseDB.updateTag(this.node,key,tags+','+data.tag);
                        }
                    }
                }
            ]
        });
        alertTagSet.setTitle('Tags');
        alertTagSet.present();
    }

    addReminder(sender, sender_worker, message, date, key) {
        this.navCtrl.push(AddReminderPage, {
            "sender": sender,
            "sender_worker": sender_worker,
            "message": message,
            "date": date,
            "key": key,
            "worker_id": this.worker_id,
            "node": this.node
        });
    }

    markasFollowUp(sender, sender_is_worker, message, date, key) {
        this.apiService.setFollow(sender, sender_is_worker, message, date, key, this.node, this.worker_id).then(response => {
            if (response[0] == "FollowUp add success") {
                this.showToastSuccess(response[0])
            }
            if (response[0] == "In list following") {
                this.showToastError(response[0])
            }
        }, error => {
            this.showToastError(JSON.stringify(error));
        })
    }

    showToastSuccess(notification) {
        const toast = this.toastCtrl.create({
            message: notification,
            duration: 3000,
            cssClass: "bg-success",
            position: 'top',
        });
        toast.present();
    }

    showToastError(notification) {
        const toast = this.toastCtrl.create({
            message: notification,
            duration: 3000,
            cssClass: "bg-danger",
            position: 'top',
        });
        toast.present();
    }

    directMessage(value) {
        let toRead = _.map(_.filter(value, {'node': this.node}), 'recipient');
        if (toRead == this.myId) {
            this.firebaseDB.deleteMessage(this.node);
        }
    }

    tagsFormattedCount(tags: string){
        if(isDefined(tags)){
            return tags.split(",").length;
        } else {
            return 0;
        }
    }

}