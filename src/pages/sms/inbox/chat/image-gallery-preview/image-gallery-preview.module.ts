import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImageGalleryPreviewPage } from './image-gallery-preview';
import {PipesModule} from "../../../../../pipes/pipes.module";

@NgModule({
  declarations: [
    ImageGalleryPreviewPage,
  ],
  imports: [
    IonicPageModule.forChild(ImageGalleryPreviewPage),
      PipesModule
  ],
})
export class ImageGalleryPreviewPageModule {}
