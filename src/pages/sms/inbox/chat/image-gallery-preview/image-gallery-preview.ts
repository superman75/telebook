import { Component } from '@angular/core';
import {IonicPage, ViewController, NavParams, LoadingController} from 'ionic-angular';
import {FirebaseServiceProvider} from "../../../../../providers/firebase-service/firebase-service";

@IonicPage()
@Component({
  selector: 'page-image-gallery-preview',
  templateUrl: 'image-gallery-preview.html',
})
export class ImageGalleryPreviewPage {

  image64:string;
  image:string;
  message:string=' ';
  origin:string;
  constructor(public viewCtrl: ViewController,
              public navParams: NavParams,
              private fireDB: FirebaseServiceProvider,
              private loadingCtrl: LoadingController) {
    this.image64 = this.navParams.get('image64');
    this.image = this.navParams.get('image');
    this.origin = this.navParams.get('origin');
  }
    close(value){
      if(value){
        if(this.origin == 'phone'){
            let loading = this.loadingCtrl.create({
                content: 'Uploading file, please wait...',
                dismissOnPageChange: true
            });
            loading.present();
          this.fireDB.uploadFile(this.image).then(data=>{
              loading.dismiss();
            this.viewCtrl.dismiss({image: data, message: this.message, params: true});
          })
        }else{
            this.viewCtrl.dismiss({image: this.image, message: this.message, params: true});
        }
      }else{
        this.viewCtrl.dismiss({params: false})
      }
    }
}
