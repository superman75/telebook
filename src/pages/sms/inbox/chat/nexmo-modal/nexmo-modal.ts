import { Component } from '@angular/core';
import {IonicPage, NavParams, ViewController} from 'ionic-angular';
import {ApiServiceProvider} from "../../../../../providers/api-service/api-service";

@IonicPage()
@Component({
  selector: 'page-nexmo-modal',
  templateUrl: 'nexmo-modal.html',
})
export class NexmoModalPage {
    customer_id: any;
    nexmo_data: any;

    constructor(public navParams: NavParams,
                private viewCrl: ViewController,
                private apiServices: ApiServiceProvider) {
        this.customer_id = this.navParams.get('customer_id');
        this.getCustomerInfoById();
    }

    dismiss() {
        this.viewCrl.dismiss();
    }

    getCustomerInfoById() {
        this.apiServices.getCustomerInfoById(this.customer_id).then(response => {
            let formatter = JSON.parse(response["customers"].nexmo_json);
            this.nexmo_data = this.syntaxHighlight(JSON.stringify(formatter, null, 4));
        })
    }

    syntaxHighlight(json) {
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match.replace(/['"]+/g, '').replace(/_/g,' ') + '</span>';
        }).replace(/{/g,'').replace(/},/g,'').replace(/}/g,'').replace(/,/g,'');
    }
}
