import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NexmoModalPage } from './nexmo-modal';

@NgModule({
  declarations: [
    NexmoModalPage,
  ],
  imports: [
    IonicPageModule.forChild(NexmoModalPage),
  ],
})
export class NexmoModalPageModule {}
