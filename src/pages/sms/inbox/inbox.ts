import {Component} from '@angular/core';
import {IonicPage, LoadingController, ModalController, NavController, NavParams} from 'ionic-angular';
import {ChatPage, UserNumberModalPage} from "../../pages";
import {ApiServiceProvider} from "../../../providers/api-service/api-service";
import {StorageServiceProvider} from "../../../providers/storage-service/storage-service";
import * as _ from 'lodash'
import {isDefined, isUndefined} from "ionic-angular/util/util";
import {FirebaseServiceProvider} from "../../../providers/firebase-service/firebase-service";

//import { setTimeout } from 'timers';

@IonicPage()
@Component({
    selector: 'page-inbox',
    templateUrl: 'inbox.html',
})
export class InboxPage {

    //models
    worker_id: number = null;
    did_number: number = null;
    person_name: string = null;
    worker_person: string = null;
    inbox: any = [];
    messageUnseen: any = [];
    listKeyUnseen: any = [];
    verify: boolean = false;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private apiService: ApiServiceProvider,
                private loadingCtrl: LoadingController,
                private storageCtrl: StorageServiceProvider,
                public modalCtrl: ModalController,
                private fireDB: FirebaseServiceProvider) {
        this.verify = true;
        this.worker_id = this.navParams.get('worker_id');
        this.person_name = this.navParams.get('person_name');
        this.did_number = this.navParams.get('did_number');
        this.inbox = [];
        let loading = this.loadingCtrl.create({
            content: 'Loading, please wait...'
        });
        loading.present();
        this.apiService.getListMessage(this.worker_id).then(data => {
            if (data) {
                this.storageCtrl.list_message = data;
                this.storageCtrl.save_storage(this.storageCtrl.list_message, 'list_message');
                this.fireDB.getChat('wasnotseen').subscribe(data => {
                        this.messageUnseen = data;
                        this.setUnseenMessage();
                        this.declareMessage();
                        loading.dismiss();
                    }, error => {
                        console.log(error);
                    }
                );
            } else {
                this.inbox = [];
                loading.dismiss();
            }
        }).catch(()=>{
            this.inbox = [];
            loading.dismiss();
        });
    }

    //methods
    goTochat(customer_phone_number, node, external_conversation_id, priority_id, worker_person, customer_id) {
        this.navCtrl.push(ChatPage, {
            'customer_phone_number': customer_phone_number,
            'node': node,
            'external_conversation_id': external_conversation_id,
            'worker_id': this.worker_id,
            'priority_id': priority_id,
            'person_name': this.person_name,
            'customer_id': customer_id,
            'did_number': this.did_number,
            'worker_person': worker_person
        })
    }

    declareMessage() {
        this.inbox = this.storageCtrl.list_message;
        this.setUnseentoInbox();
        this.inbox = _.orderBy(this.inbox, 'date', 'desc');
    };

    openModal() {
        let number_profile = this.modalCtrl.create(UserNumberModalPage, {worker_id: this.worker_id});
        number_profile.present();
        number_profile.onDidDismiss(params => {
            if (params.params) {
                this.goTochat(
                    params.data['recipient_number'],
                    params.data['node'],
                    params.data['external_conversation_id'],
                    null, null, null);
            }
        })
    }

    setUnseenMessage() {
        Object.keys(this.storageCtrl.list_message).forEach(key => {
            this.listKeyUnseen[key] = this.findKeyUnseenMessage(this.storageCtrl.list_message[key]['node']);
        })
    }

    findKeyUnseenMessage(value) {
        let objeto = _.find(this.messageUnseen, {node: value});
        if (isUndefined(objeto)) {
            return {count: 0, date: 0}
        } else {
            return {count: objeto["count"], date: objeto["date"]}
        }
    }
    setUnseentoInbox() {
        Object.keys(this.inbox).forEach(key => {
            _.assignIn(this.inbox[key], this.listKeyUnseen[key]);
            this.fireDB.getLastMessage(this.inbox[key]["node"]).subscribe(data => {
                if (isDefined(data[0])) {
                    _.assignIn(this.inbox[key], {last_message: data[0]["message"]});
                }
            });
        })
    }
}
