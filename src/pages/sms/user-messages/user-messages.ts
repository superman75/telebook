import {Component} from '@angular/core';
import {
    IonicPage,
    NavController,
    NavParams
} from 'ionic-angular';
import { ChatPage, InboxPage} from "../../pages";
import {ApiServiceProvider} from "../../../providers/api-service/api-service";



@IonicPage()
@Component({
  selector: 'page-user-messages',
  templateUrl: 'user-messages.html',
})
export class UserMessagesPage {
    messages = "viewInbox";
    //inbox
    worker_id: number = null;
    did_number: number = null;
    person_name: string = null;
    worker_person: string = null;
    inbox: any = [];
    verify: boolean = false;

    inboxPage = InboxPage;
    chatPage = ChatPage;

    data = {
        person_name: null,
        node: null,
        worker_id: null,
        internal_conversation_id: null,
        did_number: null,
        direct_count: null,
    };

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private apiService: ApiServiceProvider) {
        this.data.worker_id = this.navParams.get("worker_id");
        this.data.person_name = this.navParams.get("person_name");
        this.data.node = this.navParams.get("node");

        this.data.internal_conversation_id = this.navParams.get("internal_conversation_id");
        this.data.did_number = this.navParams.get("did_number");
        this.data.direct_count = this.navParams.get("direct_count");

        this.verify = true;
        this.worker_id = this.navParams.get('worker_id');
        this.person_name = this.navParams.get('person_name');
        this.did_number = this.navParams.get('did_number');
        this.inbox = [];
        if(this.data.node == null){
            this.newNode(this.data.worker_id);
        }
    }

    newNode(recipient_id){
        this.apiService.newNode(recipient_id).then(data=>{
            this.data.node = data["node"];
        })
    }
}
