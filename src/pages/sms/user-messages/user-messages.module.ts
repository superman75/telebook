import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserMessagesPage } from './user-messages';
import {PipesModule} from "../../../pipes/pipes.module";
import {ComponentsModule} from '../../../components/components.module'

@NgModule({
  declarations: [
    UserMessagesPage,
  ],
  imports: [
    IonicPageModule.forChild(UserMessagesPage),
      PipesModule,
      ComponentsModule
  ],
})
export class UserMessagesPageModule {}
