import { Component} from '@angular/core';
import { IonicPage, LoadingController, NavController } from 'ionic-angular';
import * as _ from 'lodash';
import { UserMessagesPage} from "../pages";
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import { StorageServiceProvider } from "../../providers/storage-service/storage-service";
import { FirebaseServiceProvider } from "../../providers/firebase-service/firebase-service";
import {isDefined, isUndefined} from "ionic-angular/util/util";

@IonicPage()
@Component({
    selector: 'page-sms',
    templateUrl: 'sms.html',
})
export class SmsPage {

    constructor(public navCtrl: NavController,
        private apiService: ApiServiceProvider,
        private storageCtrl: StorageServiceProvider,
        private loadingCtrl: LoadingController,
        private fireDB: FirebaseServiceProvider) {
        this.mainMethod();
    }

    //MODELS
    contact = [];
    message_unseen: any = [];
    myId: any;
    user_messages = UserMessagesPage;
    //METHODS
    ionViewWillEnter() {
        this.fireDB.updateCurrentSession(this.storageCtrl.user_info.user.id,'Sms');
    }
    mainMethod() {
        const loader = this.loadingCtrl.create({
            content: "Please wait...",
        });
        loader.present();
        this.myId = this.storageCtrl.user_info.user.id;
        this.apiService.getUserMessenger().then((data) => {
            this.storageCtrl.messenger_info = this.apiService.addUrlProfilePhoto(data);
            this.storageCtrl.save_storage(this.storageCtrl.messenger_info, 'messenger_info');
            this.fireDB.getChat('wasnotseen').subscribe(data => {
                this.message_unseen = data;
                this.declareContact();
            });
            loader.dismiss()
        }).catch(()=>{
            loader.dismiss();
            this.contact = [];
        })
    }

    declareContact() {
        this.contact = [];
        this.contact = this.storageCtrl.messenger_info;
        this.assignMessage(this.contact);
        this.contact = _.orderBy(this.contact, 'date', 'asc');
    }
    getItem(va) {
        this.declareContact();
        const val = va.target.value;
        if (val && val.trim() != '') {
            this.contact = this.contact.filter((item) => {
                return item.person_name.toLowerCase().indexOf(val.toLowerCase()) > -1
            })
        }
    };

    doRefresh(refresher) {
        this.mainMethod();
        refresher.complete();
    }

    orderList(value) {
        this.contact = _.orderBy(this.contact, value.field, value.order);
    }
    assignMessage(data){
        let contact_by_type = _.groupBy(this.message_unseen,'type');
        let search_by_Customer = [];
        let search_by_Worker = [];
        data.forEach(data=>{
            if(isDefined(contact_by_type.Customer)){
                search_by_Customer = _.find(contact_by_type.Customer,["recipient", data.worker_id]);
                if(isDefined(search_by_Customer)){
                    _.assign(data, {"count": search_by_Customer["count"], "external_node": search_by_Customer["node"], "date": search_by_Customer["date"] });
                }
            }else{
                _.assign(data, {"count": 0, "external_node": "0-0-Null"});
            }
            if(isDefined(contact_by_type.Worker)){
                search_by_Worker = _.find(contact_by_type.Worker,{"sender": data.worker_id,"recipient": this.myId});
                if(isUndefined(search_by_Worker)){
                    _.assign(data, {"direct_count": 0});
                }else{
                    _.assign(data, {"direct_count": search_by_Worker["count"], "date": search_by_Worker["date"]});
                }
            }else{
                _.assign(data, {"direct_count": 0});
            }
        });
        this.contact = data;
    }
}