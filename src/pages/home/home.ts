import { Component } from '@angular/core';
import {NavController } from 'ionic-angular';
import {CalldetailPage } from "../pages";
import { AlertController } from 'ionic-angular';
import {ApiServiceProvider} from "../../providers/api-service/api-service";
import {GrouplistPage } from "../pages";
import {CallsPage } from "../pages";
import {RemindersPage } from "../pages";
import {FollowupPage } from "../pages";
import {AgentsonlinePage } from "../pages";
import {FirebaseServiceProvider} from "../../providers/firebase-service/firebase-service";
import * as _ from 'lodash'
import {StorageServiceProvider} from "../../providers/storage-service/storage-service";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  date: any = new Date();

  constructor(public navCtrl: NavController, public alertCtrl: AlertController,
    private apiService: ApiServiceProvider, private fBD: FirebaseServiceProvider, private storageCtrl: StorageServiceProvider) {

  }
//models
  calls:any;
  followUp: any;
  groups:any;
  total_online:number;
  reminderReturn:any;
  countReminder: any;

  ionViewWillEnter(){
      //ESTOY ONLINE
      this.total_online = 0;
      this.fBD.updateCurrentSession(this.storageCtrl.user_info.user.id,'Login');
      this.apiService.getGroup().then(data=>{
      if(data!=null){
        this.groups = _.size(data["groups"]);  
      }
      });
      this.apiService.getFollowup().then(data=>{
          this.followUp = _.size(data["follow_ups"]);
          this.getOnlineAgents();
          this.getReminders();
      });
  }
  goTocalldetail(call)
  {
    this.navCtrl.push(CalldetailPage,{call: call});
  }

  showConfirmBlackList() {
    const confirm = this.alertCtrl.create({
      title: 'Are you sure?',
      message: 'the contact will be added to the blacklist',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  goTogrouplist()
  {
    this.navCtrl.push(GrouplistPage)
  }
  
  goToCalls()
  {
    this.navCtrl.push(CallsPage)
  }

  goTofollowUp()
  {
    this.navCtrl.push(FollowupPage)
  }
  goToAgentsOnline()
  {
    this.navCtrl.push(AgentsonlinePage)
  }
  goToReminder(){
    this.navCtrl.push(RemindersPage)
  }

  getOnlineAgents(){
      this.fBD.getChat('online').subscribe(result=>{
          this.total_online = 0;
          let now = this.fBD.currentDate();
          Object.keys(result).forEach(key=> {
              let difference = now - result[key]['date'];
              if((difference <= 900000 && difference >=-60000) && result[key]['user_id']!=this.storageCtrl.user_info.user.id){ //SOLO pintar si es mayor a 15 min
                  this.total_online++;
              }
          });
      })
  }
 
  getReminders(){
    let today=new Date();
    let year=today.getFullYear();
    let mm=today.getMonth()+1;
    let day=today.getDate();

    let mes=mm.toString().length;
    let dia=day.toString().length;
    
    let mesf:any;
    let diaF:any;

    if(mes<=1){mesf="0"+mm}
    if(dia<=1){diaF="0"+day}else{diaF=day}
    let datetoday=year+"-"+mesf+"-"+diaF;
    this.apiService.getReminders().then(response=>{
    this.reminderReturn = response["reminders"];
    this.countReminder = _.size(this.reminderReturn);
  },error=>{
  console.log(error);
  })
 }

}
