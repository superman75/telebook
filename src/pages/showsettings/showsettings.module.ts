import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowsettingsPage } from './showsettings';
import {ComponentsModule} from '../../components/components.module'

@NgModule({
  declarations: [
    ShowsettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowsettingsPage),
      ComponentsModule
  ],
})
export class ShowsettingsPageModule {}
