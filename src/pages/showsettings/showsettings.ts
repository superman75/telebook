import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {SettingspasswordPage } from "../pages";
import {SettingPage } from "../pages";
import {GrouplistPage } from "../pages";
import {ConfigurationPage } from "../pages";
import {FirebaseServiceProvider} from "../../providers/firebase-service/firebase-service";
import {StorageServiceProvider} from "../../providers/storage-service/storage-service";
import {Platform} from "ionic-angular";
import {BackgroundMode} from "@ionic-native/background-mode";


@IonicPage()
@Component({
  selector: 'page-showsettings',
  templateUrl: 'showsettings.html',
})
export class ShowsettingsPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private fb: FirebaseServiceProvider,
              private storageCtrl: StorageServiceProvider,
              private platform: Platform,
              private backgroundMode: BackgroundMode,
              private alertCtrl: AlertController) {
  }

  ionViewWillEnter() {
      this.fb.updateCurrentSession(this.storageCtrl.user_info.user.id,'Setting');
  }

  goToProfile()
  {
    this.navCtrl.push(SettingPage)
  }
  goToPassword()
  {
    this.navCtrl.push(SettingspasswordPage)
  }
  goTogrouplist()
  {
    this.navCtrl.push(GrouplistPage)
  }
  goToconfiguration()
  {
    this.navCtrl.push(ConfigurationPage)
  }
  closeConfirm(){
    const close = this.alertCtrl.create({
      title: 'Close APP',
      message: 'Do you want to close the APP?',
        buttons:[
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Close App',
              handler:()=>{
                this.closeApp();
              }
            }
        ]
    });
    close.present();
  }
  closeApp(){
      if(this.platform.is('cordova')){
          this.backgroundMode.disable();
          this.fb.updateCurrentSession(this.storageCtrl.user_info.user.id,'Offline');
          this.platform.exitApp();
      }else{
        console.log('se ha cerrado');
          this.fb.updateCurrentSession(this.storageCtrl.user_info.user.id,'Offline');
      }
  }
}
