import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalldetailPage } from './calldetail';
import { ComponentsModule } from '../../components/components.module'

@NgModule({
  declarations: [
    CalldetailPage,
  ],
  imports: [
    IonicPageModule.forChild(CalldetailPage),
      ComponentsModule
  ],
})
export class CalldetailPageModule {}
