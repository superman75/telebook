import {Component} from '@angular/core';
import {IonicPage, NavParams} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-calldetail',
  templateUrl: 'calldetail.html',
})

export class CalldetailPage {
    call:any = [];
  constructor(public navParams: NavParams) {
   this.call = this.navParams.get('call');
  }

  ionViewWillLeave(){
      this.call.recording = null;
  }
}
