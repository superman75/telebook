import { Injectable } from '@angular/core';
import {ImagePicker} from "@ionic-native/image-picker";
import {FirebaseServiceProvider} from "../firebase-service/firebase-service";
import {ImageGalleryPreviewPage} from "../../pages/sms/inbox/chat/image-gallery-preview/image-gallery-preview";
import {ModalController} from "ionic-angular";

@Injectable()
export class NativeProvider {

  constructor(public imageCtrl: ImagePicker,
              public firebaseDB: FirebaseServiceProvider,
              public modalCtrl: ModalController) {
  }
  findImageFromPhone(){
    return new Promise((resolve)=>{
        this.imageCtrl.hasReadPermission().then((result)=>{
            if(result){
              this.openGallery().then(data=>{
                resolve(data);
              })
            }else{
                this.imageCtrl.requestReadPermission().then(()=>{
                    this.openGallery().then(data=>{
                        resolve(data);
                    })
                })
            }
        });
    })
  }
  openGallery(){
    return new Promise((resolve)=>{
        this.firebaseDB.openGallery().then(result=>{
            this.firebaseDB.convertImage(result).then(result=>{
                this.openPreview('data:image/jpeg;base64,'+result, 'profile', result).then(data=>{
                  resolve(data);
                });
            })
        });
    });
  }
  openPreview(image64, origin, image){
    return new Promise((resolve)=>{
        let preview = this.modalCtrl.create(ImageGalleryPreviewPage,{image64: image64, origin: origin, image: image});
        preview.present();
        preview.onDidDismiss(data=>{
            if(data.params){
                resolve(data);
            }
        })
    })
  }
}
