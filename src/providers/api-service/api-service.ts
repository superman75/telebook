import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {StorageServiceProvider} from "../storage-service/storage-service";
import {VariablesServiceProvider} from "../variables-service/variables-service";
import {DatePipe} from '@angular/common';

@Injectable()
export class ApiServiceProvider {

    constructor(public http: HttpClient, private datePipe: DatePipe,
                private storage: StorageServiceProvider,
                private variables: VariablesServiceProvider) {
    }

    login(email, pass) {
        return new Promise((resolve, reject) => {
            this.http.post(this.variables.urlApi + 'signin',
                {'email': email, 'password': pass}).subscribe(data => {
                resolve(data);
            }, error => {
                console.log('error');
                reject(error)
            })
        })
    }

    sendMessage(data) {
        return new Promise((resolve, reject) => {
            this.http.post(this.variables.urlApi + 'send?token=' + this.storage.user_info.token,
                data).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error)
            })
        })
    }

    sendMessageChat(data) {
        return new Promise((resolve, reject) => {
            this.http.post(this.variables.urlApi + 'chat?token=' + this.storage.user_info.token,
                data).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error)
            })
        })
    }

    getUserMessenger() {
        return new Promise((resolve, reject) => {
            this.http.get(
                this.variables.urlApi + 'internal_conversations?token=' + this.storage.user_info.token + '&response=array')
                .subscribe(data => {
                    resolve(data);
                }, error => {
                    reject(error);
                })
        })
    }

    addUrlProfilePhoto(data) {
        for (let index of data) {
            index.avatar = this.variables.urlStorageImagesFirebase + index.username + '?alt=media';
        }
        return data;
    }

    getProfilePhoto(username) {
        return this.variables.urlStorageImagesFirebase + username + '?alt=media';
    }

    getListMessage(worker_id) {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'external_conversations?token=' + this.storage.user_info.token + '&worker_id=' + worker_id).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            })
        })
    }

    getChatFromNumberFound(phone_number, recipient_id, sender_id) {
        return new Promise((resolve, reject) => {
            this.http.post(this.variables.urlApi + 'external_conversations?token=' + this.storage.user_info.token, {
                phone_number: phone_number,
                recipient_id: recipient_id,
                sender_id: sender_id
            }).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            })
        })
    }

    getNumberFinder(data) {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'customers?token=' + this.storage.user_info.token + '&search=' + data + '&response=array').subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            })
        })
    }

    getCustomerInfoById(id) {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'customers/' + id + '?token=' + this.storage.user_info.token).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            })
        })
    }

    recoverPassword(email) {
        return new Promise((resolve, reject) => {
            this.http.post(this.variables.urlApi + 'password/email', email).subscribe(data => {
                resolve(data)
            }, error => {
                reject(error)
            })
        })
    }

    changeGroup(idGroup: number, idAdmin?: number) {
        return new Promise((resolve, reject) => {
            this.http.put(this.variables.urlApi + 'profile/groups/' + idGroup + '?profileid=' + idAdmin + '&token=' + this.storage.user_info.token, {}).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            })
        })
    }

    updateProfile(name, url) {
        return new Promise(resolve => {
           let body = {
                profile_image: name,
                profile_image_url: url
            };
            this.http.put(this.variables.urlApi + 'profile/image_profile?token=' + this.storage.user_info.token, body)
                .subscribe(response => {
                    resolve(response);
                })
        })
    }

    getGroup() {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'profile/groups?token=' + this.storage.user_info.token).subscribe(data => {
                resolve(data);
                // {"groups":[
                //     {"id":1,"name":"Group 1","status":0},
                //     {"id":2,"name":"Group 2","status":1}
                //     ]
                // }
            }, error => {
                reject(error);
            })
        })
    }

    getGroupDetails(id) {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'groups' + '/' + id + '?response=array&token=' + this.storage.user_info.token).subscribe(data => {
                resolve(data);
                /*
                groups: { //array de uno
                    id: 1,
                    name: "groupName"
                }
                admins: { //array
                    id: 1,
                    person: "adminsName",
                    status: 1
                },
                workers:{
                    id: 2,
                    person: "workerName"
                }
                 */
            }, error => {
                reject(error);
            })
        })
    }

    getCalls(page: number) {
        if (page >= 2 && Number.isInteger(page)) {
            return new Promise((resolve, reject) => {
                this.http.get(this.variables.urlApi + 'calls?token=' + this.storage.user_info.token + '&limit=10&page=' + page + '&response=array').subscribe(data => {
                    resolve(data);
                }, error => {
                    reject(error);
                })
            })
        }
        else {
            return new Promise((resolve, reject) => {
                this.http.get(this.variables.urlApi + 'calls?token=' + this.storage.user_info.token + '&limit=10&page=1&response=array').subscribe(data => {
                    resolve(data);
                }, error => {
                    reject(error);
                })
            })
        }
    }

    getUserData() {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'users/' + this.storage.user_info.user.id + '/edit?token=' + this.storage.user_info.token).subscribe(data => {
                resolve(data);
            })
        })
    }

    getCytiByState(id_state) {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'users/' + this.storage.user_info.user.id + '/edit?token=' + this.storage.user_info.token + '&state_id=' + id_state).subscribe(data => {
                resolve(data);
            })
        })
    }

    getStatesByCountry(id_country) {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'users/' + this.storage.user_info.user.id + '/edit?token=' + this.storage.user_info.token + '&country_id=' + id_country).subscribe(data => {
                resolve(data);
            })
        })
    }

    updateDataUser(data) {
        console.log("parametros" + data)
        return new Promise((resolve, reject) => {
            this.http.put(this.variables.urlApi + 'users/' + this.storage.user_info.user.id + '?token=' + this.storage.user_info.token,
                data).subscribe(data => {
                resolve(data);
            }, error => {
                console.log('error');
                reject(error)
            })
        })
    }

    updatePassword(data) {
        return new Promise((resolve, reject) => {
            this.http.post(this.variables.urlApi + 'profile/password?token=' + this.storage.user_info.token,
                data).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error.error)
            })
        })
    }

    getFollowup() {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'follow_ups?token=' + this.storage.user_info.token).subscribe(data => {
                resolve(data);
            })
        })
    }

    deleteFollowUp(id) {
        return new Promise((resolve, reject) => {
            this.http.delete(this.variables.urlApi + 'follow_ups/' + id + '?token=' + this.storage.user_info.token).subscribe(data => {
                resolve(data);
            })
        })
    }

    setPriority(data) {
        return new Promise((resolve, reject) => {
            this.http.post(this.variables.urlApi + 'set_priority?token=' + this.storage.user_info.token, data
            ).subscribe(data => {
                resolve(data);
            }, error => {
                console.log('error');
            })
        })
    }

    getAllUsers() {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'users?token=' + this.storage.user_info.token + '&response=array').subscribe(data => {
                resolve(data);
            }, error => {
                reject(error)
            })
        })
    }

    getMessagepredefinid() {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'quick_answers' + '?token=' + this.storage.user_info.token).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error)
            })
        })
    }

    getSenderDetaillMessage(id) {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'external_conversations/' + id + '?token=' + this.storage.user_info.token).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error)
            })
        })
    }

    getReminders() {
        return new Promise((resolve, reject) => {
            this.http.get(this.variables.urlApi + 'reminders?token=' + this.storage.user_info.token).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error)
            })
        })
    }

    deleteReminder(id) {
        return new Promise((resolve, reject) => {
            this.http.delete(this.variables.urlApi + 'reminders/' + id + '?token=' + this.storage.user_info.token).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            })
        })
    }

    setFollow(sender, sender_is_worker, message, date, key, node, worker_id) {
        let text: any;
        if (message) {
            text = message
        } else {
            text = "no text"
        }
        const sender_type = sender_is_worker ? 'App\\Worker' : 'App\\Customer';
        const _date = new Date(date * 1000);
        console.log(_date);
        const datesms = this.datePipe.transform(_date, 'yyyy-MM-dd hh:mm:ss');
        const data = {
            node: node,
            key_token: key,
            token: this.storage.user_info.token,
            worker_id: worker_id,
            sender_id: sender,
            sender_type: sender_type,
            text: text,
            datesms: datesms
        };
        return new Promise((resolve, reject) => {
            this.http.post(this.variables.urlApi + 'follow_ups?token=' + '&limit=5' + '&page=1' + this.storage.user_info.token, data
            ).subscribe(data => {
                resolve(data);
            }, error => {
                console.log('error');
            })
        })
    }

    setReminder(node, token, datereminder, note, sender, sender_is_worker, message, date, worker_id) {
        console.log(node, token, datereminder, note, sender, sender_is_worker, message, date, worker_id);
        let text: any;
        if (message) {
            text = message
        } else {
            text = "no text"
        }
        const sender_type = sender_is_worker ? 'App\\Worker' : 'App\\Customer';
        const _date = new Date(date * 1000);
        const datesms = this.datePipe.transform(_date, 'yyyy-MM-dd hh:mm:ss');
        const data = {
            node: node,
            token: token,
            datereminder: datereminder,
            note: note,
            sender_id: sender,
            worker_id: worker_id,
            sender_type: sender_type,
            text: text,
            datesms: datesms
        };
        return new Promise((resolve, reject) => {
            this.http.post(this.variables.urlApi + 'reminders?key_token=' + this.storage.user_info.token, data
            ).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error)
            })
        })
    }

    newNode(recipient_id) {
        return new Promise((resolve, reject) => {
            this.http.post(this.variables.urlApi + 'internal_conversations?token=' + this.storage.user_info.token, {recipient_id}
            ).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            })
        })
    }
    clickToCall(fromNumber, toNumber){
        return new Promise((resolve, reject) => {
            this.http.post(this.variables.urlApi + 'call/twilioclicktocall' + '?token=' + this.storage.user_info.token, {
                from: fromNumber,
                to: toNumber
            }).subscribe(response => {
                resolve(response);
            }, error => {
                reject(error);
            })
        })
    }
}
