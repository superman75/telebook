import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import * as firebase from 'firebase';
import {ImagePicker, ImagePickerOptions} from "@ionic-native/image-picker";
import {Base64} from "@ionic-native/base64";
import {isUndefined} from "ionic-angular/util/util";

@Injectable()
export class FirebaseServiceProvider {
  constructor(private afDB: AngularFireDatabase,
              private base64: Base64,
              private gallery: ImagePicker) {
  }

  isTyping:any;
  key:any=null;
  name:any;

  getChat4(node){
    return this.afDB.list(node).snapshotChanges();
  }
  getChat(node){
    return this.afDB.list(node).valueChanges();  
  }

  uploadFileNew(archivo, fileName?, urlReference?:string){
      return new Promise((resolve,reject)=>{
          let storeRef = firebase.storage().ref();
          if(isUndefined(fileName)){
              fileName = Date.now().toString();
          }
          if(isUndefined(urlReference)){
              urlReference = 'gallery/';
          }
          let imageRef = storeRef.child(urlReference+fileName);
          imageRef.putString(archivo,'base64',{contentType: 'image/jpeg'}).then(()=>{
              imageRef.getDownloadURL().then(url=>{
                  resolve({url: url, name: fileName});
              });
          })
      })
  }
  uploadFile(archivo){
      return new Promise((resolve,reject)=>{
          let storeRef = firebase.storage().ref();
          let nombreArchivo:string = Date.now().toString();
          let uploadTask: firebase.storage.UploadTask =
              storeRef
                  .child('gallery/'+nombreArchivo)
                  .putString(archivo, 'base64',{contentType: 'image/jpeg'});
          uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,()=>{
              //% de porcentaje subidos
          },(error)=>{
              //manejo de errores
              console.log('ERROR en la carga');
              console.log(JSON.stringify(error));
              reject(error);
          },()=>{
              storeRef.child('gallery/'+nombreArchivo).getDownloadURL().then(url=>{
                  resolve(url);
              });
          })
      })
  }

  deleteImage(name){
      return new Promise(()=>{
          let storeRef = firebase.storage().ref();
          storeRef.child('profile-image/'+name).delete();
      })
  }
    getProfileUrl(name){
      return new Promise((resolve,reject)=>{
          let storeRef = firebase.storage().ref();
          storeRef.child('profile-image/'+name).getDownloadURL().then(url=>{
              resolve(url);
          },error=>{
              reject(error);
          });
      })
    }
  deleteMessage(node_chat){
      firebase.database().ref('wasnotseen').orderByChild('node').equalTo(node_chat).once('value',function(snapshot){
          snapshot.forEach(data=>{
              data.ref.remove()
          })
      })
  }
  getLastMessage(node){
        return this.afDB.list(node,ref => ref.orderByKey().limitToLast(1)).valueChanges();
    }
  async convertImage(path){
      let file = await this.base64.encodeFile(path);
        return file.substring("data:image/*;charset=utf-8;base64,".length);
    }
  openGallery(){
      return new Promise((resolve,reject)=>{
          let options:ImagePickerOptions = {
              maximumImagesCount: 1,
              quality: 50
          };
          this.gallery.getPictures(options).then(results=>{
              for(var i=0;i<results.length;i++){
                resolve(results[i]);
              }
          })
      })
    }
  pushNode(node,data){
    firebase.database().ref().child(node).push(data);
  }
  getMyOnlineAgentsData(id){
    return new Promise((resolve, reject) => {
        firebase.database().ref('online').orderByChild('user_id').equalTo(id).on('child_added', function (snapshot) {
            resolve(snapshot.ref);
        })
    })
    }
  updateCurrentSession(id,event){
      let date = this.currentDate();
      firebase.database().ref('online').orderByChild('user_id').equalTo(id).on('child_added',function(snapshot){
          snapshot.ref.update({date: date,last_event:event})
      });
    }

  IsTyping(node,fireID,data){
      this.afDB.list('IsTyping/'+node).update(fireID,{isTyping: data});
    }
  getTyping(node){
        return this.afDB.list('IsTyping/'+node,ref => ref.orderByChild('isTyping').equalTo(true)).valueChanges();
    }
  currentDate(){
      return Date.now();
    }

  checkTyping(node:string,loginUser:string){ //check the ID where my name is present
        return new Promise((resolve,reject)=> {
            firebase.database().
            ref("IsTyping").
            child(node).
            orderByChild('name').
            equalTo(loginUser).once('value', function (snapshot) {
                if(snapshot.val()==null){
                    let typing: any = {
                        name: loginUser,
                        isTyping: false
                    };
                    firebase.database().ref('IsTyping/'+node).push(typing).then(snapshot=>{
                        resolve(snapshot.key); //return firebase ID created recently
                    })
                }else{
                    let allID = [];
                    snapshot.forEach((data)=>{
                        allID.push(data.key)
                    });
                    resolve(allID[0]); //return firebase ID where my name is present, the first
                }
            })
        });
    }
  updateTag(node,key,tags){
      this.afDB.list(node).update(key,{tags: tags});
  }
}
