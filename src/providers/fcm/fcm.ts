import { Injectable } from '@angular/core';
import { Firebase } from '@ionic-native/firebase'
import { Platform} from "ionic-angular";
import { AngularFireDatabase} from "angularfire2/database";
import { StorageServiceProvider} from "../storage-service/storage-service";

@Injectable()
export class FcmProvider {

  constructor(
      public firebaseNative: Firebase,
      public afd: AngularFireDatabase,
      public platform: Platform,
      public storage: StorageServiceProvider) {
  }
  async getToken(){
      this.firebaseNative.subscribe('allUser').then((response)=>{
        console.log(response)
      });
  }
  listenNotifications(){
    return this.firebaseNative.onNotificationOpen()
  }
}
