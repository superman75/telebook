import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { Platform } from "ionic-angular";

/*
  Generated class for the StorageServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageServiceProvider {

  user_info:any = [];
  messenger_info:any = [];
  list_message:any = [];
  external_conversation:any=[];
  _priority:any;

  constructor(private platform: Platform, private storage: Storage) {
    console.log('Hello StorageServiceProvider Provider');
  }
  load_storage(data_string){
      return new Promise((resolve,reject)=>{
          if(this.platform.is('cordova')){ //device
            this.storage.ready().then(()=>{
              this.storage.get(data_string).then((val)=>{
                if(val){
                  resolve(val);
                }
                resolve()
              })
            })
          }else{ //desktop
              if(localStorage.getItem(data_string)){
                  resolve(JSON.parse(localStorage.getItem(data_string)));
              };
              resolve()
          }
    })
  }
  save_storage(data,string){
    if(this.platform.is('cordova')){ //device
      this.storage.ready().then(()=>{
        this.storage.set(string,data);
      })
    }else{ //desktop
      localStorage.setItem(string,JSON.stringify(data));
    }
  }
  delete_storage(data_string){
    if(this.platform.is('cordova')){
      this.storage.ready().then(()=>{
        this.storage.remove(data_string);
        })
    }else{
        localStorage.removeItem(data_string);
    }
  }
}
