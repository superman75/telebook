import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { DatePipe } from '@angular/common';

import { PipesModule} from "../pipes/pipes.module";
//PAGES
import { HomePage } from '../pages/pages';
import { TabsPage } from '../pages/pages';
import { SmsPageModule } from '../pages/sms/sms.module';
import { LoginPageModule } from "../pages/login/login.module";
import { SettingPageModule } from '../pages/setting/setting.module';
import { PassrecoveryPageModule } from '../pages/passrecovery/passrecovery.module';
import { UserMessagesPageModule } from "../pages/sms/user-messages/user-messages.module";
import { ChatPageModule } from '../pages/sms/inbox/chat/chat.module';
import { NexmoModalPageModule } from "../pages/sms/inbox/chat/nexmo-modal/nexmo-modal.module";
import { InboxPageModule } from '../pages/sms/inbox/inbox.module';
import { ShowsettingsPageModule } from '../pages/showsettings/showsettings.module';
import { SettingspasswordPageModule } from '../pages/setting/settingspassword/settingspassword.module';
import { GrouplistPageModule } from '../pages/grouplist/grouplist.module';
import { GroupdetailsPageModule } from '../pages/grouplist/groupdetails/groupdetails.module';
import { CalldetailPageModule } from '../pages/calldetail/calldetail.module';
import { UserNumberModalPageModule } from '../pages/sms/inbox/user-number-modal/user-number-modal.module';
import { CallsPageModule } from '../pages/calls/calls.module';
import { FollowupPageModule } from '../pages/followup/followup.module';
import { ConfigurationPageModule } from '../pages/setting/configuration/configuration.module';
import { GalleryPhotoPageModule } from "../pages/setting/configuration/gallery-photo/gallery-photo.module";
import { PhotoListPageModule } from "../pages/setting/configuration/gallery-photo/photo-list/photo-list.module";
import { ImageGalleryPreviewPageModule } from "../pages/sms/inbox/chat/image-gallery-preview/image-gallery-preview.module";
import { SenderDetailPageModule } from "../pages/sender-detail/sender-detail.module";
import { RemindersPageModule } from "../pages/reminders/reminders.module";
import { ReminderPageModule } from "../pages/reminders/reminder/reminder.module";


//PLUGINS
import { IonicStorageModule } from "@ionic/storage";
import { AngularFireModule} from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { Network } from "@ionic-native/network";
import { Push } from '@ionic-native/push'
import { Firebase} from "@ionic-native/firebase";
import {Base64} from '@ionic-native/base64'
import {BackgroundMode} from '@ionic-native/background-mode';
//COMPONENTS
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//PIPES Y PROVIDERS
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { StorageServiceProvider } from '../providers/storage-service/storage-service';
import { FirebaseServiceProvider } from '../providers/firebase-service/firebase-service';
import { VariablesServiceProvider,firebaseConfig } from '../providers/variables-service/variables-service';
import { ImagePicker } from "@ionic-native/image-picker";
import { FcmProvider } from '../providers/fcm/fcm';
import {ComponentsModule} from "../components/components.module";
import { OrderByComponent } from '../components/order-by/order-by';
import {AddReminderPageModule} from "../pages/add-reminder/add-reminder.module";
import {AgentsonlinePageModule} from "../pages/agentsonline/agentsonline.module";
import { NativeProvider } from '../providers/native/native';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    OrderByComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{ tabsHideOnSubPages: true,scrollPadding: false}),
    HttpClientModule,
    IonicStorageModule.forRoot(),
      AngularFireModule.initializeApp(firebaseConfig),
      PipesModule,
      AngularFireDatabaseModule,
      AngularFireAuthModule,
      ComponentsModule,
      AddReminderPageModule,
      AgentsonlinePageModule,
      CalldetailPageModule,
      CallsPageModule,
      SmsPageModule,
      UserMessagesPageModule,
      InboxPageModule,
      LoginPageModule,
      ChatPageModule,
        NexmoModalPageModule,
      ImageGalleryPreviewPageModule,
      UserNumberModalPageModule,
      SettingPageModule,
      PassrecoveryPageModule,
      ShowsettingsPageModule,
      SettingPageModule,
      SettingspasswordPageModule,
      GrouplistPageModule,
        GroupdetailsPageModule,
      FollowupPageModule,
      ConfigurationPageModule,
      GalleryPhotoPageModule,
      PhotoListPageModule,
      SenderDetailPageModule,
      RemindersPageModule,
        ReminderPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    OrderByComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiServiceProvider,
    AngularFireDatabase,
      Firebase,
    StorageServiceProvider,
    FirebaseServiceProvider,
    VariablesServiceProvider,
    Network,
    ImagePicker,
      Push,
    FcmProvider,
    Base64,
    BackgroundMode,
      DatePipe,
    NativeProvider
  ]
})
export class AppModule {
}
