import { Component } from '@angular/core';
import { Platform, ToastController} from 'ionic-angular';
import {tap} from "rxjs/operators";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from "@ionic-native/network";
import { BackgroundMode} from "@ionic-native/background-mode";
import { LoginPage } from '../pages/pages';
import { TabsPage } from '../pages/pages';

import {StorageServiceProvider} from "../providers/storage-service/storage-service";
import {FirebaseServiceProvider} from "../providers/firebase-service/firebase-service";
import { FcmProvider } from "../providers/fcm/fcm";
import { Push} from '@ionic-native/push';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private storageCtrl: StorageServiceProvider,
              private network: Network,
              private toastCtrl: ToastController,
              private fireService: FirebaseServiceProvider,
              private fcm: FcmProvider,
              private push: Push,
              private backgroundMode: BackgroundMode) {
      this.backgroundMode.enable();
      let toast =  this.toastCtrl.create({
          message:'No connection, please reconnect',
          position: 'top',
          cssClass: 'bg-danger'
      })
      this.network.onDisconnect().subscribe(()=>{
         toast.present();
      });
      this.network.onConnect().subscribe(()=>{
          toast.dismiss();
      })
      this.storageCtrl.load_storage('user_info').then((data)=>{
          if(data){
              if(data['token'] == null){
                  this.rootPage = LoginPage;
              }else {
                  if (this.verifyToken(data['token'])) { //si el token es valido
                      this.rootPage = TabsPage;
                      // this.rootPage = SmsPage;
                      this.storageCtrl.user_info = data;
                      this.iddleFunctions(data["user"].id)
                  } else {
                      this.rootPage = LoginPage;
                  }
              }
          }else{
              this.rootPage = LoginPage;
          }
      });
      this.fireService.getChat('wasnotseen').subscribe(data=>{
          this.storageCtrl.save_storage(data,'wasnotseen');
      });
      // this.pushSetup();
      if(platform.is('cordova')){
          platform.ready().then(()=>{
              fcm.getToken();
              fcm.listenNotifications().pipe(
                  tap(msg=>{
                      const toast = this.toastCtrl.create({
                          message: msg.body,
                          duration: 3000,
                      });
                      toast.present();
                  })
              );
          })
      }
      statusBar.styleDefault();
      splashScreen.hide();
  }
  verifyToken(token){
    let base64Url = token.split('.')[1];
    let base64 = base64Url.replace('-','+').replace('_','/');
    let token_info = JSON.parse(window.atob(base64));
    token_info = token_info.exp;
    if(token_info > (Date.now()/1000)){
      return true;
    }else{
      return false;
    }
  }
  iddleFunctions(id){
      this.fireService.getMyOnlineAgentsData(id).then(result=>{
          console.log(result["user_id"]);
      })
  }
}