import { NgModule } from '@angular/core';
import { NavHeaderComponent } from './nav-header/nav-header';
import { ChatComponent } from './chat-component/chat-component';
import {IonicModule} from "ionic-angular";
@NgModule({
	declarations: [NavHeaderComponent,
    ChatComponent],
	imports: [IonicModule],
	exports: [NavHeaderComponent,
    ChatComponent]
})
export class ComponentsModule {}
