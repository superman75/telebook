import { Component } from '@angular/core';
import { ViewController } from "ionic-angular";

@Component({
  selector: 'order-by',
  templateUrl: 'order-by.html'
})
export class OrderByComponent {

  constructor(public viewCtrl: ViewController) {
  }
  sendValue(value){
    this.viewCtrl.dismiss(value);
  }
}
