import {Component, Input, ViewChild} from '@angular/core';
import {
    ActionSheetController, AlertController,
    Content,
    LoadingController,
    ModalController,
    NavController,
    NavParams, ToastController
} from "ionic-angular";
import {FirebaseServiceProvider} from "../../providers/firebase-service/firebase-service";
import {ApiServiceProvider} from "../../providers/api-service/api-service";
import {StorageServiceProvider} from "../../providers/storage-service/storage-service";
import {ImagePicker} from "@ionic-native/image-picker";
import {isDefined, isUndefined} from "ionic-angular/util/util";
import * as _ from "lodash";
import {PhotoListPage} from "../../pages/setting/configuration/gallery-photo/photo-list/photo-list";
import {ImageGalleryPreviewPage} from "../../pages/sms/inbox/chat/image-gallery-preview/image-gallery-preview";
import {SenderDetailPage} from "../../pages/sender-detail/sender-detail";
import {AddReminderPage} from "../../pages/add-reminder/add-reminder";
import { UnixToHumanPipe} from "../../pipes/unix-to-human/unix-to-human";

/**
 * Generated class for the ChatComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'chat-component',
  templateUrl: 'chat-component.html',
    providers:[UnixToHumanPipe]
})
export class ChatComponent {
    @Input() priority_id: number;
    @Input() node: string;
    @Input() external_conversation_id: number;
    @Input() internal_conversation_id: number;
    @Input() did_number: number;
    @Input() worker_id: number;
    @Input() person_name: string;
    @Input() worker_person: string;
    @Input() customer_id: number;
    @Input() direct_count: number;
    @Input() origin: string;

    title: any;
    chat_message: any = [];
    message_written: any = '';
    mms: boolean = true;
    quicklyanswers: any;
    myTypingId: any;
    userTyping: any = [];
    myName: any;
    myId: any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private loadingCtrl: LoadingController,
              private firebaseDB: FirebaseServiceProvider,
              private apiService: ApiServiceProvider,
              private modalCtrl: ModalController,
              private userInfo_external_conversation: StorageServiceProvider,
              private imageCtrl: ImagePicker,
              private actionCtrl: ActionSheetController,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private storageCtrl: StorageServiceProvider,
              private unixCtrl: UnixToHumanPipe) {
  }
  ngOnInit(){
      this.myId = this.storageCtrl.user_info.user.id;
      const loading = this.loadingCtrl.create({
          content: "Please wait...",
      });
      loading.present();
      this.myName = this.storageCtrl.user_info.user.name;
      this.delete_message(this.node);
      this.firebaseDB.getChat4(this.node).subscribe(data => {
          this.chat_message = [];
          data.forEach(data => {
              this.chat_message.push(_.assign(data.payload.val(), {"token": data.key}));
          });
          this.userInfo_external_conversation.external_conversation = this.external_conversation_id;
          this.scrollBotton();
      });
      loading.dismiss();
      this.getquicklymessage();
      this.checkTyping();
      this.firebaseDB.getTyping(this.node).subscribe(data => {
          this.userTyping = data;
      });
      this.initMethod();
  }
    getquicklymessage() {
        this.apiService.getMessagepredefinid().then((data) => {
            this.quicklyanswers = data['answers'];
        }, error => {
            console.log(error);
        });
    }

    //METODOS
    sendMessageWritten(imageURL, message) {
        let identi;
        if (isDefined(this.external_conversation_id)) {
            identi = this.external_conversation_id;
        } else {
            identi = this.internal_conversation_id;
        }
        let data = {
            id: identi,
            imageURL: imageURL,
            message: message,
            type: null
        };
        if (isDefined(this.internal_conversation_id)) {
            this.apiService.sendMessageChat(data);
        } else {
            this.apiService.sendMessage(data);
        }
        this.message_written = '';
        setTimeout(() => {
            this.scrollBotton();
        }, 1100);
    }

    sendDefaultMessage() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Quickly Answers');
        if (this.quicklyanswers) {
            Object.keys(this.quicklyanswers).forEach((key) => {
                alert.addInput({
                    type: 'radio',
                    label: this.quicklyanswers[key]['answer'],
                    checked: false,
                    value: this.quicklyanswers[key]['answer']
                });
            })
        } else {
            alert.setSubTitle("Quick answers not available")
        }
        alert.addButton('Cancel');
        alert.addButton({
            text: 'Send',
            handler: data => {
                if (data) {
                    this.sendMessageWritten(null, data);
                }
            }
        });
        alert.present();
    }
    initMethod() {
        setTimeout(() => {
            this.scrollBotton();
            this.delete_message(this.node)
        }, 1100);
    }

    ionViewDidEnter() {
        if (this.did_number == null) {
            this.toastCtrl.create({
                message: "You can't send message until you assign a Did to this User",
                position: 'bottom',
                cssClass: 'bg-danger',
                dismissOnPageChange: true,
                showCloseButton: true
            }).present();
        }
    }

    findImageFromGallery() {
        let imageGallery = this.modalCtrl.create(PhotoListPage, {id: this.worker_id, chat: true});
        imageGallery.onDidDismiss(data => {
            if (data != null) {
                this.openPreview('data:image/jpeg;base64,' + data, 'gallery', data)
            }
        });
        imageGallery.present();
    }

    findImageFromPhone() {
        this.imageCtrl.hasReadPermission().then((result) => {
            if (result) {
                this.openGallery();
            } else {
                this.imageCtrl.requestReadPermission().then(() => {
                    this.openGallery();
                })
            }
        });
    }

    findImage() {
        let actionSheet = this.actionCtrl.create({
            title: 'Choose your source',
            buttons: [
                {
                    text: 'Telabook Gallery',
                    icon: 'cloud-download',
                    handler: () => {
                        this.findImageFromGallery();
                    }
                },
                {
                    text: 'My Phone Images',
                    icon: 'cloud-upload',
                    handler: () => {
                        this.findImageFromPhone();
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                }
            ]
        });
        actionSheet.present();
    }

    openPreview(image64, origin, image) {
        let preview = this.modalCtrl.create(ImageGalleryPreviewPage, {image64: image64, origin: origin, image: image});
        preview.present();
        preview.onDidDismiss(data => {
            if (data.params) {
                this.sendMessageWritten(data["image"], data["message"]);
            }
        })
    }

    delete_message(node) {
        if (this.direct_count > 0 || isUndefined(this.direct_count)) {
            this.firebaseDB.deleteMessage(node);
            this.direct_count = 0;
        }
    }

    openGallery() {
        this.firebaseDB.openGallery().then(result => {
            this.firebaseDB.convertImage(result).then(result => {
                this.openPreview('data:image/jpeg;base64,' + result, 'phone', result)
            })
        });
    }

    scrollBotton() {
        let element = document.querySelector('#chatComponent');
        element.scrollTo({ top: element.scrollHeight, behavior: 'smooth' });
    }

    checkTyping() {
        this.firebaseDB.checkTyping(this.node, this.myName).then(data => {
            this.myTypingId = data; //if null, I never have written something in this chat, return firebase ID
        });
    }

    showOption(sender, sender_is_worker, message, date, key, tags, type) {
        let alert = this.alertCtrl.create();
        alert.setTitle('Please select an option');
        if(type == 'worker'){
            alert.addInput({
                type: 'radio',
                label: 'Sender Details',
                checked: false,
                value: '1'
            });
        }
        alert.addInput({
            type: 'radio',
            label: 'Mark as FollowUp',
            checked: false,
            value: '2'
        });
        alert.addInput({
            type: 'radio',
            label: 'Set Reminder',
            checked: false,
            value: '3',
        });
        alert.addInput({
            type: 'radio',
            label: 'See tags',
            checked: false,
            value: '4',
        });
        alert.addInput({
            type: 'radio',
            label: 'Set new Tag',
            checked: false,
            value: '5',
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'Ok',
            handler: data => {
                switch (data) {
                    case "1":
                        this.navCtrl.push(SenderDetailPage, {
                            "external_conversation_id": this.external_conversation_id,
                            "sender": sender,
                            "person_name": this.person_name,
                            "did_number": this.did_number
                        });
                        break;
                    case "2":
                        this.markasFollowUp(sender, sender_is_worker, message, date, key);
                        break;
                    case "3":
                        this.addReminder(sender, sender_is_worker, message, date, key);
                        break;
                    case "4":
                        alert.dismiss();
                        this.seeTags(tags,key);
                        return false;
                    case "5":
                        alert.dismiss();
                        this.setTags(tags,key);
                        return false;
                    default:
                        break;
                }
            }
        });
        alert.present();
    }

    seeTags(tags,key){
        let alertTag;
        if(isDefined(tags)){
            alertTag = this.alertCtrl.create();
            alertTag.setTitle('Tags');
            let alltag = tags.split(',');
            alltag.forEach(data => {
                alertTag.addInput({
                    type: 'checkbox',
                    label: data,
                    checked: false,
                    value: data,
                });
            });
            alertTag.addButton({
                text: 'Remove',
                handler: data => {
                    if(isDefined(data)){
                        let tagsArray = tags.split(',');
                        let newTags = _.pullAll(tagsArray,data);
                        this.firebaseDB.updateTag(this.node,key,newTags.length > 0 ? newTags.toString() : null);
                    }
                }
            });
            alertTag.addButton({
                text: 'Ok',
            });
        } else {
            alertTag = this.alertCtrl.create({
                title: "Tags",
                subTitle: "No tags",
                buttons: ["OK"]
            })
        }
        alertTag.present();
    }

    setTags(tags,key){
        let alertTagSet = this.alertCtrl.create({
            title: "New tag",
            message: "Add your tags's description, you can add multiple tags separating it by comma.",
            inputs: [
                {
                    name: 'tag',
                    placeholder: "Tag's Description"
                },
            ],
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: "Save",
                    handler: data => {
                        if(isUndefined(tags)){
                            this.firebaseDB.updateTag(this.node,key,data.tag);
                        } else {
                            this.firebaseDB.updateTag(this.node,key,tags+','+data.tag);
                        }
                    }
                }
            ]
        });
        alertTagSet.setTitle('Tags');
        alertTagSet.present();
    }

    addReminder(sender, sender_worker, message, date, key) {
        this.navCtrl.push(AddReminderPage, {
            "sender": sender,
            "sender_worker": sender_worker,
            "message": message,
            "date": date,
            "key": key,
            "worker_id": this.worker_id,
            "node": this.node
        });
    }

    markasFollowUp(sender, sender_is_worker, message, date, key) {
        this.apiService.setFollow(sender, sender_is_worker, message, date, key, this.node, this.worker_id).then(response => {
            if (response[0] == "FollowUp add success") {
                this.showToastSuccess(response[0])
            }
            if (response[0] == "In list following") {
                this.showToastError(response[0])
            }
        }, error => {
            this.showToastError(JSON.stringify(error));
        })
    }

    showToastSuccess(notification) {
        const toast = this.toastCtrl.create({
            message: notification,
            duration: 3000,
            cssClass: "bg-success",
            position: 'top',
        });
        toast.present();
    }

    showToastError(notification) {
        const toast = this.toastCtrl.create({
            message: notification,
            duration: 3000,
            cssClass: "bg-danger",
            position: 'top',
        });
        toast.present();
    }

    directMessage(value) {
        let toRead = _.map(_.filter(value, {'node': this.node}), 'recipient');
        if (toRead == this.myId) {
            this.firebaseDB.deleteMessage(this.node);
        }
    }

    tagsFormattedCount(tags: string){
        if(isDefined(tags)){
            return tags.split(",").length;
        } else {
            return 0;
        }
    }

    unixHuman(text){
      return this.unixCtrl.transform(text);
    }
}
