import { Component, Input, Output, EventEmitter } from '@angular/core';
import {App, ModalController, PopoverController, AlertController, NavController, ToastController} from 'ionic-angular'
import {LoginPage, NexmoModalPage, SmsPage} from "../../pages/pages"
import {isDefined, isUndefined} from "ionic-angular/util/util";
import { StorageServiceProvider } from "../../providers/storage-service/storage-service";
import { OrderByComponent } from "../order-by/order-by";
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import { FirebaseServiceProvider } from "../../providers/firebase-service/firebase-service";
import * as _ from 'lodash';


@Component({
  selector: 'nav-header',
  templateUrl: 'nav-header.html'
})
export class NavHeaderComponent {
  @Input('title') textTitle;
  @Input('disconnect') boolDisconnect;
  @Input('orderBy') orderBy: boolean;
  @Input('setFlat') setFlat: boolean;
  @Input('alertmessage') alertmessage: boolean;
  @Input('nexmo') nexmo: number;
  @Input('click-to-call') click_to_call: string;
  @Output() orderEvent = new EventEmitter<string>();
  @Output() directMessage = new EventEmitter<object>();

  text: string;
  disconnect: boolean;
  dataFlat: any = {};
  inbox: any = [];
  myId: number;
  count_message: number;
  count_message_direct: number;
  num_priority: any = null;
  constructor(public appCtrl: App,
              private storageCtrl: StorageServiceProvider,
              private popCtrl: PopoverController,
              public AlertController: AlertController,
              public userService: ApiServiceProvider,
              public navCtrl: NavController,
              private fireDB: FirebaseServiceProvider,
              private modalCtrl: ModalController,
              private toastCtrl: ToastController) {

    this.fireDB.getChat('wasnotseen').subscribe(data => {
      this.myId = this.storageCtrl.user_info.user.id;
      let unseen;
      unseen = _.groupBy(data,'type');
      if(isDefined(unseen.Customer)){
          this.count_message = _.sumBy(unseen.Customer,'count');
      }
      if(isDefined(unseen.Worker)){
        this.directMessage.emit(unseen.Worker);
        setTimeout(()=>{
          this.count_message_direct = _.sumBy(_.filter(unseen.Worker,["recipient", this.myId]),'count');
        },5000);
      }
    });
    this.num_priority = this.storageCtrl._priority;
  }
  ngAfterViewInit() {
    this.text = this.textTitle;
    this.disconnect = this.boolDisconnect;
    if (isUndefined(this.boolDisconnect)) {
      this.disconnect = true;
    } else {
      this.disconnect = this.boolDisconnect;
    }
  }
  logOut() {
    this.storageCtrl.delete_storage('user_info');
    this.storageCtrl.delete_storage('list_message');
    this.storageCtrl.delete_storage('messenger_info');
    this.fireDB.updateCurrentSession(this.storageCtrl.user_info.user.id,'Offline');
    this.appCtrl.getRootNav().setRoot(LoginPage);
  }
  orderByTrigger(event) {
    let pop = this.popCtrl.create(OrderByComponent);
    pop.present({ ev: event })
    pop.onDidDismiss(value => {
      if(value != null){
          this.orderEvent.emit(value);
      }
    })
  }
  showFlat() {
    console.log(this.num_priority);
    let alert = this.AlertController.create();
    alert.setTitle('Set the priority ');
    if (this.num_priority == null) {
      alert.addInput({
        type: 'radio',
        label: 'None Priority',
        checked: true,
        value: this.num_priority
      });
      alert.addInput({
        type: 'radio',
        label: 'Low Priority',
        checked: false,
        value: '1'
      });
      alert.addInput({
        type: 'radio',
        label: 'Medium Priority',
        checked: false,
        value: '2',
      });
      alert.addInput({
        type: 'radio',
        label: 'High Priority',
        checked: false,
        value: '3',
      });
    }
    if (this.num_priority == 2) {
      alert.addInput({
        type: 'radio',
        label: 'Medium Priority',
        checked: true,
        value: '2',
      });
      alert.addInput({
        type: 'radio',
        label: 'Low Priority',
        checked: false,
        value: '1'
      });
      alert.addInput({
        type: 'radio',
        label: 'High Priority',
        checked: false,
        value: '3',
      });
      alert.addInput({
        type: 'radio',
        label: 'None Priority',
        checked: false,
        value: this.num_priority,
      });
    }
    if (this.num_priority == 3) {
      alert.addInput({
        type: 'radio',
        label: 'High Priority',
        checked: true,
        value: '3',
      });
      alert.addInput({
        type: 'radio',
        label: 'Medium Priority',
        checked: false,
        value: '2',
      });
      alert.addInput({
        type: 'radio',
        label: 'Low Priority',
        checked: false,
        value: '1'
      });
      alert.addInput({
        type: 'radio',
        label: 'None Priority',
        checked: false,
        value: this.num_priority,
      });
    }
    if (this.num_priority == 1) {
      alert.addInput({
        type: 'radio',
        label: 'Low Priority',
        checked: true,
        value: '1'
      });
      alert.addInput({
        type: 'radio',
        label: 'Medium Priority',
        checked: false,
        value: '2',
      });
      alert.addInput({
        type: 'radio',
        label: 'High Priority',
        checked: false,
        value: '3',
      });
      alert.addInput({
        type: 'radio',
        label: 'None Priority',
        checked: false,
        value: this.num_priority,
      });
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'Ok',
      handler: data => {
        if (data) {
          this.dataFlat.external_conversation_id = this.storageCtrl.external_conversation;
          this.dataFlat.priority = data;
          this.userService.setPriority(this.dataFlat);
        }
      }
    });
    alert.present();
  }
  goToSms() {
    this.navCtrl.push(SmsPage);
  }
  showNexmo(){
        this.modalCtrl.create(NexmoModalPage,{'customer_id':this.nexmo}).present();
    }
    clickToCall(){
      const confirm = this.AlertController.create({
          title: "Are you sure do you want to call to this number?",
          message: "Click yes to proceed",
          buttons:[
              {
                  text: 'Cancel',
              },
              {
                  text: 'Proceed',
                  handler: () => {
                    this.storageCtrl.load_storage('user_info').then(response=>{
                      this.userService.clickToCall(response["user"].phone,this.click_to_call).then(response=>{
                          const toast = this.toastCtrl.create({
                              message: response["message"],
                              duration: 5000,
                              position: "top"
                          });
                          toast.present();
                      })
                    });
                  }
              }
          ]
      });
      confirm.present();
    }
}
